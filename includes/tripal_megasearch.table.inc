<?php

use Drupal\Core\Render\Markup;

use Drupal\chado_search\Core\SessionVar;
use Drupal\chado_search\Core\SearchDatabase;
use Drupal\chado_search\Core\Sql;
use Drupal\chado_search\Result\Table;
use Drupal\chado_search\Result\Pager;

/*
 * Create download
 */
function tripal_megasearch_ajax_create_table ($form, &$form_state) {
  $where = array();
  $where [] = Sql::repeatableText('conditions', $form_state);
  return tripal_megasearch_ajax_get_table ($form, $form_state, $where);
}

/*
 * Apply filters and generate file
 */
function tripal_megasearch_ajax_get_table ($form, &$form_state, $conditions = array(), $page = 0, $num_per_page = 10, $headers = NULL, $order = NULL, $autoscroll = TRUE) {
  $num_per_page = \Drupal::state()->get('tripal_megasearch_result_rows', '10');
  $values = $form_state->getValues();
  $mview = $values['datatype'];

  $where = '';
  // Remove NULL filters
  $valid_cond = array();
  foreach ($conditions AS $cond) {
    if ($cond && trim($cond)) {
      $valid_cond [] = $cond;
    }
  }
  $num_cond = count($valid_cond);
  if ($num_cond > 0) {
    $where = ' WHERE ';
  }
  $index = 0;
  foreach ($valid_cond AS $cond) {
    $where .= $cond . ' ';
    if ($index < $num_cond - 1) {
      $where .= ' AND ';
    }
    $index ++;
  }
  // Gather selected fields
  $fields = $form_state->getValue('attribute_checkboxes');
  $selected_cols = '';
  foreach ($fields AS $col) {
    if ($col) {
      $selected_cols .= $col . ',';
    }
  }
  $selected_cols = trim($selected_cols, ',');
  if (!$selected_cols) {
    return "<div class=\"tripal_megasearch-info\">No field selected. Please select at least one field.</div>";
  }

  $default_duplicates = \Drupal::state()->get('tripal_megasearch_result_duplicates', 'not_remove');
  $override = chado_search_get_mview_setting('tripal_megasearch_data_definition', $mview, 'duplicates');
  if($override) {
    $default_duplicates = $override;
  }

  $checkbox_filter = chado_search_get_mview_setting('tripal_megasearch_data_definition', $mview, 'checkbox_filter');
  if ($checkbox_filter) {
    SessionVar::setSessionVar('tripal_megasearch', 'checkbox-filter', $checkbox_filter);
    SessionVar::setSessionVar('tripal_megasearch', 'checkbox-filter-data', []);
  }
  else {
    SessionVar::delSessionVar('tripal_megasearch', 'checkbox-filter');
  }

  $link_callbacks =  chado_search_get_mview_setting('tripal_megasearch_data_definition', $mview, 'field_link_callback');
  $count_col = chado_search_get_mview_setting('tripal_megasearch_data_definition', $mview, 'count');
  $dbdef = SearchDatabase::getColumns($mview);
  $dbcols = array();
  foreach ($dbdef AS $def) {
    $dbcols [$def->column_name] = $def->column_name;
  }
  $missing_cols = array();
  if ($link_callbacks) {
    foreach ($link_callbacks AS $callback) {
      $cb = explode(':', $callback);
      if (count($cb) > 1) {
        $vars = explode(',', $cb[1]);
        foreach ($vars AS $var) {
          if (!key_exists($var, $fields) && key_exists($var, $dbcols) && $var != $count_col) {
            $missing_cols [] = $var;
          }
        }
      }
    }
  }
  if ($count_col) {
    $missing_cols [] = $count_col;
  }

  $statement = $selected_cols;
  if ($default_duplicates == 'remove') {
    $exceed_dup_limit = FALSE;
    $default_dup_limit = \Drupal::state()->get('tripal_megasearch_duplicate_limit', '1000000');
    if ($default_dup_limit != 0) {
      $result_rows = chado_search_query("SELECT count(*) FROM {" . $mview . "}" . $where, array(), 'public,chado')->fetchField();
      if ($result_rows > $default_dup_limit) {
        $exceed_dup_limit = TRUE;
      }
    }
    if (!$exceed_dup_limit) {
      $statement = " DISTINCT $selected_cols";
      if (count($missing_cols) > 0) {
        $statement = "DISTINCT ON ($selected_cols) $selected_cols";
      }
    }
    else {
      \Drupal::messenger()->addMessage('Duplicate rows not removed due to large amount of data returned.');
    }
  }
  foreach ($missing_cols AS $ms) {
    $statement .= ",$ms";
  }
  $sql ="SELECT $statement FROM {" . $mview . "}" . $where;
  SessionVar::setSessionVar('tripal_megasearch','sql', $sql);
  SessionVar::setSessionVar('tripal_megasearch', 'autoscroll', TRUE);
  $sql_total = "SELECT count(*) FROM (" . $sql . ") T";
  // if we only want to count unique value of a certain column:

  if ($count_col) {
    $sql_total = "SELECT count($count_col) FROM (" . $sql . ") T";
  }
  $total = chado_search_query($sql_total, array(), 'public,chado')->fetchField();
  // Make sure there is at least one result
  if ($total == 0) {
    return "<div class=\"tripal_megasearch-info\">No result. Please adjust the query and try again.</div>";
  }
  else {
    SessionVar::setSessionVar('tripal_megasearch','total-items', $total);
  }
  // Limit download attributes
  $headers = chado_search_get_mview_setting('tripal_megasearch_data_definition', $mview, 'field');
  $checkboxes = $values['attribute_checkboxes'];
  foreach ($checkboxes AS $key => $val) {
    if ($val == '0') {
      unset($headers[$key]);
    }
  }
  //Make sure at least one attribute is selected
  if (count($headers) == 0) {
    return "<div class=\"tripal_megasearch-info\">Nothing to show. Please select at least one field.</div>";
  }

  $sortheader = array();
  foreach ($headers AS $key => $val) {
    if (isset($link_callbacks[$key])) {
      $sortheader[$key . ':s:' . $link_callbacks[$key]] = $val;
    }
    else {
      $sortheader[$key . ':s'] = $val;
    }
  }
  SessionVar::setSessionVar('tripal_megasearch', 'default-headers', $sortheader);

  $lsql = $sql . " LIMIT $num_per_page";
  $result = chado_search_query($lsql, array(), 'public,chado');
  $total_pages =Pager::totalPages($total, $num_per_page);

  $div =
  "<div id=\"tripal_megasearch-result-summary\" class=\"chado_search-result-summary\">
            <div id=\"tripal_megasearch-result-count\" class=\"chado_search-result-count\">
              <strong>$total</strong> records were returned
            </div></div>";
  // If all checks passed, create the download file
  $link_new_tab = \Drupal::state()->get('tripal_megasearch_link_in_new_tab', 'off') == 'on' ? TRUE : FALSE;
  $tab = new Table('tripal_megasearch', $result, 0, $num_per_page, $sortheader, NULL, TRUE, $link_new_tab);
  $div .= $tab->getSrc();
  $pager = new Pager('tripal_megasearch', 'tripal_megasearch', $total_pages, TRUE, $num_per_page);
  $div .= $pager->getSrc();
  $div .= '<script>jQuery(\'.is-invalid\').removeClass(\'form-control\');jQuery(\'.is-invalid\').removeClass(\'is-invalid\');</script>';
  return Markup::create($div);
}