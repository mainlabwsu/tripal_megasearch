<?php

require_once 'tripal_megasearch.download.inc';
require_once 'tripal_megasearch.download.fasta.inc';
require_once 'tripal_megasearch.sequence_retrieval.inc';
require_once 'tripal_megasearch.table.inc';

use Drupal\Core\Render\Markup;

use Drupal\chado_search\Core\Set;
use Drupal\chado_search\Core\SessionVar;
use Drupal\chado_search\Core\Sql;

/*************************************************************
 * Search form, form validation, and submit function
 */
// Search form
function tripal_megasearch_query_build_form ($form) {

  $form_state =& $form->form_state;
  $storage = $form_state->getStorage();
  $storage['limit'] = \Drupal::state()->get('tripal_megasearch_download_limit', '200000');
  $storage['fasta_limit'] = \Drupal::state()->get('tripal_megasearch_fasta_download_limit', '50000');

  // Description
  $desc = t(\Drupal::state()->get('tripal_megasearch_instruction', 'Tripal MegaSearch is a tool for downloading biological data. %limits%<br><br>Select a data type to start building your own query and download data in bulk:'));
  $desc_dl = '';
  $desc_fa = '';


  if (isset($storage['limit'])) {
    $desc_dl = number_format($storage['limit']) . ' records.';
  }
  if (isset($storage['fasta_limit'])) {
    $desc_fa = number_format($storage['fasta_limit']) . ' FASTA sequences.';
  }
  if ($desc_dl || $desc_fa) {
    $desc = str_replace('%limits%', '(Current limit per download: ' . $desc_dl . ' ' . $desc_fa . ')', $desc);
  }
  else {
    $desc = str_replace('%limits%','', $desc);
  }
  $form->addMarkup(
      Set::markup()
      ->id('description')
      ->text($desc)
      ->newLine()
   );

  // Data Type
  $options = chado_search_get_all_mview_settings('tripal_megasearch_data_definition', 'name');
  asort($options);
  $form->addSelectOptionFilter(
      Set::selectOptionFilter()
      ->id('datatype')
      ->title('Data Type')
      ->options($options)
      ->noKeyConversion(TRUE)
  );
  // The default dropdown first option "Any" is not applicable to MegaSearch, change it
  $form->form['datatype']['#options'][0] = '[Select a Data Type]';

  if(isset($_GET['datatype']) && isset($options[$_GET['datatype']])) {
    $selected = $_GET['datatype'];
    $form->addMarkup(
        Set::markup()
        ->id('datatype_selected')
        ->text(
            "<script>
                jQuery(document).ready(
                  function () {
                    jQuery('#chado_search-id-datatype option[value=\"" . $selected . "\"]').attr(\"selected\", \"selected\");
                    jQuery('#chado_search-id-datatype').children('option').each(function() {
                        if (jQuery(this).is(':selected'))
                        { jQuery(this).trigger('change');  }
                    });
                  })
              </script>")
        );
  }
  
  // Reset Button
  $form->addReset(
      Set::reset()
      ->id('tripal_megasearch_reset')
      ->newLine());

  // Status & Counter
  $form->addDynamicMarkup(
    Set::dynamicMarkup()
      ->id('status')
      ->dependOnId('datatype')
      ->callback('tripal_megasearch_ajax_get_message')
      ->newLine()
  );

  // Dynamic Form
  $query_form = \Drupal::state()->get('tripal_megasearch_query_form', 'dynamic');
  if ($query_form == 'dynamic') {
    \Drupal::moduleHandler()->loadInclude('tripal_megasearch', 'inc', 'includes/tripal_megasearch.form.dynamic');
    $form = tripal_megasearch_query_form_dynamic($form);

  // Static Forms (Predefined)
  } else if ($query_form == 'static') {
    \Drupal::moduleHandler()->loadInclude('tripal_megasearch', 'inc', 'includes/tripal_megasearch.form.static');
    $form = tripal_megasearch_query_form_static($form);
  }

  $form->addMarkup(
      Set::markup()
      ->id('results')
      );
  $form->form['datatype']['#attribute']['update']['results'] = array('wrapper' => 'chado_search-tripal_megasearch-markup-results');
  return $form;
}

/*
 *  Return updated status message
 */
function tripal_megasearch_ajax_get_message($mview, $form, $form_state, $conditions = array()) {
  $message = NULL;
  if ($mview != NULL && $mview != '0') {
    $total = 0;
    $element = $form_state->getTriggeringElement();
    $applyfilters = isset($element['#id']) && $element['#id'] == 'chado_search-id-apply_filters';
    $db = \Drupal::database();
    if ($db->schema()->tableExists('chado.'. $mview) || $db->schema()->tableExists($mview)) {
      $sql_total = "SELECT count(*) FROM {" . $mview . "}";
      // if we only want to count unique value of a certain column:
      $count_col = chado_search_get_mview_setting('tripal_megasearch_data_definition', $mview, 'count');
      if ($count_col) {
        $sql_total = "SELECT count(DISTINCT $count_col) FROM {" . $mview . "}";
      }

      // apply filters
      $where = '';
      if ($applyfilters) {
        // Remove NULL filters
        $valid_cond = array();
        foreach ($conditions AS $cond) {
          if ($cond && trim($cond)) {
            $valid_cond [] = $cond;
          }
        }
        $num_cond = count($valid_cond);
        if ($num_cond > 0) {
          $where = ' WHERE ';
        }
        $index = 0;
        foreach ($valid_cond AS $cond) {
          $where .= $cond . ' ';
          if ($index < $num_cond - 1) {
            $where .= ' AND ';
          }
          $index ++;
        }
      }

      // Run SQL
      // Apply filters and get count ONLY IF there is some filter applied
      if ($where) {
        $total = chado_search_query($sql_total . $where, array(), 'chado')->fetchField();
      }
      // Get count from cache
      else {
        $total = tripal_megasearch_get_total_from_cache($mview, $sql_total);
      }
      SessionVar::setSessionVar('tripal_megasearch','total-items', $total);
    }
    $name = chado_search_get_mview_setting('tripal_megasearch_data_definition', $mview, 'name');
    $message = '<strong>'. number_format($total) . ' ' . $name . '</strong> <i>Note: actual rows in downloaded file depend on the selected fields.</i>';
  }
  else if (!isset($_GET['datatype']) || $mview == '0'){
    //return data summary
    $options = chado_search_get_all_mview_settings('tripal_megasearch_data_definition', 'name');
    asort($options);
    $rows = [];
    $sum_columns = \Drupal::state()->get('tripal_megasearch_data_summary', ['num_records' => 0, 'last_updated' =>0]);
    $header = [];
    if ($sum_columns['num_records'] || $sum_columns['last_updated']) {
      if ($sum_columns['num_records'] || $sum_columns['last_updated']) {
        $header[] = 'Data Type';
      }
      if ($sum_columns['num_records']) {
        $header[] = 'Number of Records';
      }
      if ($sum_columns['last_updated']) {
        $header[] = 'Last Updated';
      }
    }
    foreach ($options AS $mv => $dtype) {
      $stat = \Drupal::state()->get('tripal_megasearch_mview_count-' . $mv, array());
      if (isset($stat['total'])) {
        if ($sum_columns['num_records'] || $sum_columns['last_updated']) {
          $rows[$mv] = [];          
          $rows[$mv][] =  Markup::create('<a href=/tripal_megasearch?datatype=' . $mv . '>' . $dtype . '</a>');
        }
        if ($sum_columns['num_records']) {
          $rows[$mv][] = $stat['total'];          
        }
        if ($sum_columns['last_updated']) {
          $rows[$mv][] = date('m/d/Y', $stat['updated']);
        }
      }
    }
    if (count($rows) > 0) {
      $table = [
        '#type' => 'table',
        '#header' => $header,
        '#rows' => $rows,
      ];
      $message = "Data Summary:<br>" . \Drupal::service('renderer')->render($table);      
    }
  }
  return $message;
}

/**
 * Get total record count from cache if there is no filter applied
 */
function tripal_megasearch_get_total_from_cache($mview, $sql_total) {
  $db = \Drupal::database();
  $mview_time = NULL;
  /* Remove the dependency on Tripal
  if ($db->schema()->tableExists('tripal_mviews')) {
    $mview_time =
    $db->select('tripal_mviews', 'tm')
    ->fields('tm', array('last_update'))
    ->condition('name', $mview)
    ->execute()
    ->fetchField();
  }
  */
  if (!$mview_time) {
    $mview_time = \Drupal::state()->get('chado_search_time_' . $mview);
  }
  $stats = \Drupal::state()->get('tripal_megasearch_mview_count-' . $mview, array());
  $total = 0;
  if (count($stats) > 0) {
    if ($mview_time == $stats['updated']) {
      return $stats['total'];
    }
  }
  $total = chado_search_query($sql_total, array(), 'public,chado')->fetchField();
  if ($mview_time) {
    $data = array('updated' => $mview_time, 'total' => $total);
    \Drupal::state()->set('tripal_megasearch_mview_count-' . $mview, $data);
  }
  return $total;
}

/**
 * Return $form['status'] element with updated message
 */

function tripal_megasearch_get_form_status ($form, &$form_state, $where) {
  $status = $form['status'];
  $mview = $form_state->getValue('datatype');
  $message = tripal_megasearch_ajax_get_message($mview, $form, $form_state, $where);
  $status['#markup'] = Markup::create($message . '<script>jQuery(\'.chado_search-tripal_megasearch-waiting-box\').hide();jQuery(\'.is-invalid\').removeClass(\'form-control\');jQuery(\'.is-invalid\').removeClass(\'is-invalid\');</script>');
  return $status;
}

function tripal_megasearch_ajax_filter_clear_values ($form, &$form_state) {
    return $form['data_filters'];
}

/*
 * Refresh count
 */
function tripal_megasearch_ajax_refresh_count ($form, &$form_state) {
  $where = array();
  $where [] = Sql::repeatableText('conditions', $form_state);
  return tripal_megasearch_get_form_status($form, $form_state, $where);
}

function tripal_megasearch_ajax_view_result ($form, &$form_state) {
  $where = array();
  $where [] = Sql::repeatableText('conditions', $form_state);
  $result = $form['results'];
  $result['#markup'] = tripal_megasearch_ajax_get_table ($form, $form_state, $where);
  return $result;
}

/**
  Toggle select/unselect all fields
  **/
function tripal_megasearch_ajax_toggle_field_selection ($form, &$form_state) {
  $attr = $form_state->getValue('attribute_check_all');
  $checked = $attr['select_all'];
  $attributes = $form['data_attributes']['attribute_checkboxes'];
  if($checked) {
    foreach ($attributes AS $k => $v) {
      if (!preg_match('/^#.+$/', $k)) {
        $attributes[$k]['#checked'] = TRUE;
      }
    }
  }
  else {
    foreach ($attributes AS $k => $v) {
      if (!preg_match('/^#.+$/', $k)) {
        $attributes[$k]['#checked'] = FALSE;
      }
    }
  }
  return $attributes;
}

function tripal_megasearch_set_error_status($form, $message, $id = NULL) {
  $status = $form['status'];
  \Drupal::messenger()->addError($message);
  if ($id) {
    $status['#markup'] = Markup::create($status['#markup'] . '<script>jQuery(\'#chado_search-filter-tripal_megasearch-' . $id . '-field\').addClass(\'is-invalid form-control\');</script>');
  }
  return $status;
}
function tripal_megasearch_set_error_result($form, $message, $id = NULL) {
  \Drupal::messenger()->addError($message);
  $result = $form['results'];
  if ($id) {
    $result['#markup'] = Markup::create($result['#markup'] . '<script>jQuery(\'#chado_search-filter-tripal_megasearch-' . $id . '-field\').addClass(\'is-invalid form-control\');</script>');
  }
  return $result;
}
function tripal_megasearch_set_error_downlaod($form, $message, $id = NULL) {
  $status = $form['status'];
  \Drupal::messenger()->addError($message);
  if ($id) {
    $status['#markup'] = Markup::create($status['#markup'] . '<script>jQuery(\'#chado_search-filter-tripal_megasearch-' . $id . '-field\').addClass(\'is-invalid form-control\');jQuery(\'.chado_search-tripal_megasearch-waiting-box\').hide();</script>');
  }
  return $status;
}
