<?php

use Drupal\chado_search\Core\Set;
use Drupal\chado_search\Core\Sql;

function tripal_megasearch_ortholog_form ($form) {
  $form_state =& $form->form_state;
  
  // Settings for the Filter & Attribute lists (hide if datatype not selected)
  $setttings_filter =
  Set::dynamicFieldset()
  ->id('data_filters')
  ->title('Query')
  ->dependOnId('datatype');
  $setttings_attr =
  Set::dynamicFieldset()
  ->id('data_attributes')
  ->title('Downloadable Fields')
  ->dependOnId('datatype');
  if(!$form_state->getValue('datatype') || $form_state->getValue('datatype') == '0') {
    $setttings_filter->display('none');
    $setttings_attr->display('none');
  }
  
  // Filter List fieldset
  $form->addDynamicFieldset($setttings_filter);
  
  // Attribute List fieldset
  $form->addDynamicFieldset($setttings_attr);
  
  if($form_state->getValue('datatype')) {
    $mview = $form_state->getValue('datatype');
    $fields = chado_search_get_mview_setting('tripal_megasearch_data_definition', $mview, 'field');
    if (chado_search_get_mview_setting('tripal_megasearch_data_definition', $mview, 'maintenance')) {
      $form->addMarkup(Set::markup()->id('maintenance')->text('<span style="color:red">Under maintenance. Data  may not be available. Please check back later.</span>')->fieldset('data_filters'));
    }
    if ($fields) {
        $form->addButton(
            Set::button()
            ->id('tsv-download')
            ->value('TSV')
            ->fieldset('data_attributes')
            ->ajax(array(
                'callback' => 'tripal_megasearch_ajax_ortholog_create_download',
                'wrapper' => 'chado_search-filter-tripal_megasearch-status-field',
                'effect' => 'fade')
                )
            ->attributes(tripal_megasearch_generate_progress_attribute())
            );
      $form->addSubmit(
          Set::submit()
          ->value('CSV')
          ->fieldset('data_attributes')
          ->ajax(array(
            'callback' => 'tripal_megasearch_ajax_ortholog_create_download',
            'wrapper' => 'chado_search-filter-tripal_megasearch-status-field',
            'effect' => 'fade')
              )
          ->attributes(tripal_megasearch_generate_progress_attribute())
          );
      $default_rows = \Drupal::state()->get('tripal_megasearch_result_rows', '10');
      if ($default_rows > 0) {
        $form->addButton(
            Set::button()
            ->id('view_results')
            ->fieldset('data_attributes')
            ->value('View')
            ->ajax(array(
              'callback' => 'tripal_megasearch_ajax_ortholog_view_result',
              'wrapper' => 'chado_search-tripal_megasearch-markup-results',
              'effect' => 'fade')
                )
            );
      }
      $form->addButton(
          Set::button()
          ->id('apply_filters')
          ->fieldset('data_filters')
          ->value('Refresh Count')
          ->ajax(array(
            'callback' => 'tripal_megasearch_ajax_ortholog_refresh_count',
            'wrapper' => 'chado_search-filter-tripal_megasearch-status-field',
            'effect' => 'fade')
              )
          );
      $form->addButton(
          Set::button()
          ->id('clear-all-values')
          ->fieldset('data_filters')
          ->value('Clear')
          ->ajax(array(
              'callback' => 'tripal_megasearch_ajax_filter_clear_values',
              'wrapper' => 'chado_search-filter-tripal_megasearch-data_filters-field',
              'effect' => 'fade')
              )
          );
      
      // Genome
      $form->addSelectFilter(
          Set::selectFilter()
          ->id('ortholog_genome')
          ->fieldset('data_filters')
          ->title('Genome')
          ->table('tripal_megasearch_ortholog')
          ->column('l_genome')
          ->cache(TRUE)
          ->labelWidth(163)
          ->newline()
          );
      
      // Chr/scaffold
      $form->addDynamicSelectFilter(
          Set::dynamicSelectFilter()
          ->id('ortholog_chromosome')
          ->fieldset('data_filters')
          ->title('Chromosome/Scaffold')
          ->dependOnId('ortholog_genome')
          ->callback('tripal_megasearch_ortholog_ajax_chromosomes')
          ->labelWidth(163)
          ->newline()
          ->cache('tripal_megasearch_ortholog',['l_genome', 'l_landmark'])
          );
      // Location
      $form->addTextFilter(
          Set::textFilter()
          ->id('ortholog_start')
          ->title('Start')
          ->numeric(TRUE)
          ->fieldset('data_filters')
          ->labelWidth(163)
          ->newline()
          );
      $form->addTextFilter(
          Set::textFilter()
          ->id('ortholog_stop')
          ->title('Stop')
          ->numeric(TRUE)
          ->fieldset('data_filters')
          ->labelWidth(163)
          ->newline()
          );
      
      // Left feature names
      $form->addTextFilter(
          Set::textFilter()
          ->id('ortholog_feature_name')
          ->fieldset('data_filters')
          ->title('Gene/Transcript Name')
          ->labelWidth(163)
          );
      $form->addFile(
          Set::file()
          ->id('ortholog_feature_name_file_inline')
          ->fieldset('data_filters')
          ->labelWidth(1)
          ->newLine()
          );
      
      // Comparing genomes
      $form->addDynamicSelectFilter(
          Set::dynamicSelectFilter()
          ->id('ortholog_compare')
          ->fieldset('data_filters')
          ->title('Compare to')
          ->dependOnId('ortholog_genome')
          ->callback('tripal_megasearch_ortholog_ajax_comparing_genomes')
          ->labelWidth(163)
          ->multiple(TRUE)
          ->cache('tripal_megasearch_ortholog',['l_genome', 'r_genome'])
          ->newline()
          );
      
      // Comparing Chr/scaffold
      $form->addDynamicSelectFilter(
          Set::dynamicSelectFilter()
          ->id('ortholog_cp_chromosome')
          ->fieldset('data_filters')
          ->title('Chromosome/Scaffold')
          ->dependOnId('ortholog_compare')
          ->callback('tripal_megasearch_ortholog_ajax_cp_chromosomes')
          ->resetOnChange('ortholog_genome')
          ->labelWidth(163)
          ->cache('tripal_megasearch_ortholog',['r_genome', 'r_landmark'])
          ->newline()
          );
      
      // Select/unselect all fields
      $form->addCheckBoxes(
          Set::checkboxes()
          ->id('attribute_check_all')
          ->options(
              array(
                'select_all' => 'All Fields',
              )
              )
          ->ajax(
              array(
                'callback' => 'tripal_megasearch_ajax_toggle_field_selection',
                'wrapper' => 'chado_search-tripal_megasearch-checkboxes-attribute_checkboxes',
                'effect' => 'fade'
              )
              )
          ->defaultValue(array('select_all'))
          ->fieldset('data_attributes')
          );
      // Populate attribuet list
      $form->addCheckboxes(
          Set::checkBoxes()
          ->id('attribute_checkboxes')
          ->options($fields)
          ->fieldset('data_attributes')
          ->defaultValue(array_keys($fields))
          );
    }
  }
  return $form;
}

function tripal_megasearch_ajax_ortholog_get_filters(&$form_state) {
  $where = array();
  $where [] = Sql::selectFilter('ortholog_genome', $form_state, 'l_genome');
  $where [] = Sql::selectFilter('ortholog_chromosome', $form_state, 'l_landmark');
  $where [] = Sql::textFilter('ortholog_start', $form_state, 'l_fmin');
  $where [] = Sql::textFilter('ortholog_stop', $form_state, 'l_fmax');
  $where [] = Sql::textFilter('ortholog_feature_name', $form_state, 'l_feature');
  $where [] = Sql::file('ortholog_feature_name_file_inline', 'l_feature');
  $where [] = Sql::selectFilter('ortholog_compare', $form_state, 'r_genome');
  $where [] = Sql::selectFilter('ortholog_cp_chromosome', $form_state, 'r_landmark');
  return $where;
}

function tripal_megasearch_ortholog_ajax_chromosomes($val) {
  $table = \Drupal::database()->schema()->tableExists('cache_tripal_megasearch_ortholog_chromosome') ? 'cache_tripal_megasearch_ortholog_chromosome' : 'tripal_megasearch_ortholog';
  $sql = "SELECT DISTINCT l_landmark FROM {$table} WHERE l_genome = :l_genome ORDER BY l_landmark";
  return chado_search_bind_dynamic_select(array(':l_genome' => $val), 'l_landmark', $sql);
}

function tripal_megasearch_ortholog_ajax_cp_chromosomes($val) {
  $table = \Drupal::database()->schema()->tableExists('cache_tripal_megasearch_ortholog_cp_chromosome') ? 'cache_tripal_megasearch_ortholog_cp_chromosome' : 'tripal_megasearch_ortholog';
  $sql = "SELECT DISTINCT r_landmark FROM {$table} WHERE r_genome IN (:r_genome[]) ORDER BY r_landmark";
  return chado_search_bind_dynamic_select(array(':r_genome[]' => $val), 'r_landmark', $sql);
}

function tripal_megasearch_ortholog_ajax_comparing_genomes ($val) {
  $table = \Drupal::database()->schema()->tableExists('cache_tripal_megasearch_ortholog_compare') ? 'cache_tripal_megasearch_ortholog_compare' : 'tripal_megasearch_ortholog';
  $sql = "SELECT DISTINCT r_genome FROM {$table} WHERE l_genome = :l_genome ORDER BY r_genome";
  return chado_search_bind_dynamic_select(array(':l_genome' => $val), 'r_genome', $sql);
}

/*
 * Refresh count
 */
function tripal_megasearch_ajax_ortholog_refresh_count ($form, &$form_state) {  
  if ($form_state->getValue('ortholog_genome') == '0') {
    return tripal_megasearch_set_error_status($form, 'Please select a genome', 'ortholog_genome');
  }
  $where = tripal_megasearch_ajax_ortholog_get_filters($form_state);
  return tripal_megasearch_get_form_status($form, $form_state, $where);
}

function tripal_megasearch_ajax_ortholog_view_result ($form, &$form_state) {
  if ($form_state->getValue('ortholog_genome') == '0') {
    return tripal_megasearch_set_error_result($form, 'Please select a genome', 'ortholog_genome');
  }
  $where = tripal_megasearch_ajax_ortholog_get_filters($form_state);
  $result = $form['results'];
  $result['#markup'] = tripal_megasearch_ajax_get_table ($form, $form_state, $where);
  return $result;
}

/*
  * Create download
  */
function tripal_megasearch_ajax_ortholog_create_download ($form, &$form_state) {
  if ($form_state->getValue('ortholog_genome') == '0') {
    return tripal_megasearch_set_error_downlaod($form, 'Please select a genome', 'ortholog_genome');
  }
  $where = tripal_megasearch_ajax_ortholog_get_filters($form_state);
  $element = $form_state->getTriggeringElement();
  $format = $element['#value'] == 'TSV' ? 'TSV' : 'CSV';
  return tripal_megasearch_ajax_get_download_file ($form, $form_state, $where, $format);
}