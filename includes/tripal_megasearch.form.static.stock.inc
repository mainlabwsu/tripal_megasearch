<?php

use Drupal\chado_search\Core\Set;
use Drupal\chado_search\Core\Sql;

function tripal_megasearch_stock_form ($form) {
  $form_state =& $form->form_state;

  // Settings for the Filter & Attribute lists (hide if datatype not selected)
  $setttings_filter =
  Set::dynamicFieldset()
  ->id('data_filters')
  ->title('Query')
  ->dependOnId('datatype');
  $setttings_attr =
  Set::dynamicFieldset()
  ->id('data_attributes')
  ->title('Downloadable Fields')
  ->dependOnId('datatype');
  if(!$form_state->getValue('datatype') || $form_state->getValue('datatype') == '0') {
    $setttings_filter->display('none');
    $setttings_attr->display('none');
  }

  // Filter List fieldset
  $form->addDynamicFieldset($setttings_filter);

  // Attribute List fieldset
  $form->addDynamicFieldset($setttings_attr);

  if($form_state->getValue('datatype')) {
    $mview = $form_state->getValue('datatype');
    $fields = chado_search_get_mview_setting('tripal_megasearch_data_definition', $mview, 'field');
    if (chado_search_get_mview_setting('tripal_megasearch_data_definition', $mview, 'maintenance')) {
      $form->addMarkup(Set::markup()->id('maintenance')->text('<span style="color:red">Under maintenance. Data  may not be available. Please check back later.</span>')->fieldset('data_filters'));
    }
    if ($fields) {
      $form->addButton(
          Set::button()
          ->id('tsv-download')
          ->value('TSV')
          ->fieldset('data_attributes')
          ->ajax(array(
              'callback' => 'tripal_megasearch_ajax_stock_create_download',
              'wrapper' => 'chado_search-filter-tripal_megasearch-status-field',
              'effect' => 'fade')
              )
          ->attributes(tripal_megasearch_generate_progress_attribute())
          );
      $form->addSubmit(
          Set::submit()
          ->value('CSV')
          ->fieldset('data_attributes')
          ->ajax(array(
            'callback' => 'tripal_megasearch_ajax_stock_create_download',
            'wrapper' => 'chado_search-filter-tripal_megasearch-status-field',
            'effect' => 'fade')
              )
          ->attributes(tripal_megasearch_generate_progress_attribute())
          );
      $default_rows = \Drupal::state()->get('tripal_megasearch_result_rows', '10');
      if ($default_rows > 0) {
        $form->addButton(
            Set::button()
            ->id('view_results')
            ->fieldset('data_attributes')
            ->value('View')
            ->ajax(array(
              'callback' => 'tripal_megasearch_ajax_stock_view_result',
              'wrapper' => 'chado_search-tripal_megasearch-markup-results',
              'effect' => 'fade')
                )
            );
      }
      $form->addButton(
          Set::button()
          ->id('apply_filters')
          ->fieldset('data_filters')
          ->value('Refresh Count')
          ->ajax(array(
            'callback' => 'tripal_megasearch_ajax_stock_refresh_count',
            'wrapper' => 'chado_search-filter-tripal_megasearch-status-field',
            'effect' => 'fade')
              )
          );
      $form->addButton(
          Set::button()
          ->id('clear-all-values')
          ->fieldset('data_filters')
          ->value('Clear')
          ->ajax(array(
              'callback' => 'tripal_megasearch_ajax_filter_clear_values',
              'wrapper' => 'chado_search-filter-tripal_megasearch-data_filters-field',
              'effect' => 'fade')
              )
          );

      // Organism
      $form->addSelectFilter(
          Set::selectFilter()
          ->id('stock_organism')
          ->title('Organism')
          ->table('tripal_megasearch_stock')
          ->column('organism')
          ->fieldset('data_filters')
          ->labelWidth(140)
          ->multiple(TRUE)
          ->searchBox(TRUE)
          ->cache(TRUE)
          ->newline()
          );

      // Name
      $form->addTextFilter(
          Set::textFilter()
          ->id('stock_name')
          ->title('Name')
          ->labelWidth(140)
          ->fieldset('data_filters')
          );
      $form->addFile(
          Set::file()
          ->id('stock_file')
          ->title("File Upload")
          ->fieldset('data_filters')
          ->description("Provide germplasm names in a file. Separate each name by a new line.")
          );
      $form->addFieldset(
          Set::fieldset()
          ->id('stock_group_name')
          ->title('Germplasm name')
          ->startWidget('stock_name')
          ->endWidget('stock_file')
          );

      // Collection
      $form->addSelectFilter(
          Set::selectFilter()
          ->id('stock_collection')
          ->title('Collection')
          ->table('tripal_megasearch_stock')
          ->column('collection')
          ->labelWidth(140)
          ->cache(TRUE)
          ->fieldset('data_filters')
          ->newline()
          );
      $form->addTextFilter(
          Set::textFilter()
          ->id('stock_accession')
          ->title('Accession')
          ->labelWidth(140)
          ->fieldset('data_filters')
          ->newline()
          );
      $form->addFieldset(
          Set::fieldset()
          ->id('stock_group_collection')
          ->title('Collection')
          ->startWidget('stock_collection')
          ->endWidget('stock_accession')
          );
      // Parentage
      $form->addTextFilter(
          Set::textFilter()
          ->id('stock_maternal_parent')
          ->title('Maternal Parent')
          ->fieldset('data_filters')
          ->labelWidth(140)
          ->newline()
          );
      $form->addTextFilter(
          Set::textFilter()
          ->id('stock_paternal_parent')
          ->title('Paternal Parent')
          ->fieldset('data_filters')
          ->labelWidth(140)
          ->newline()
          );
      $form->addTextFilter(
          Set::textFilter()
          ->id('stock_pedigree')
          ->title('Pedigree')
          ->labelWidth(140)
          ->fieldset('data_filters')
          ->newline()
          );
      $form->addFieldset(
          Set::fieldset()
          ->id('stock_group_parentage')
          ->title('Parentage')
          ->startWidget('stock_maternal_parent')
          ->endWidget('stock_pedigree')
          );

      // Country
      $form->addSelectFilter(
          Set::selectFilter()
          ->id('stock_country')
          ->title('Country')
          ->table('tripal_megasearch_stock')
          ->column('country')
          ->labelWidth(140)
          ->fieldset('data_filters')
          ->cache(TRUE)
          ->newline()
          );
      $form->addFieldset(
          Set::fieldset()
          ->id('stock_group_country')
          ->title('Country')
          ->startWidget('stock_country')
          ->endWidget('stock_country')
          );

      // Image
      $form->addTextFilter(
          Set::textFilter()
          ->id('stock_eimage_data')
          ->title('Name')
          ->labelWidth(140)
          ->fieldset('data_filters')
          ->newline()
          );
      $form->addTextFilter(
          Set::textFilter()
          ->id('stock_legend')
          ->title('Legend')
          ->labelWidth(140)
          ->fieldset('data_filters')
          ->newline()
          );
      $form->addFieldset(
          Set::fieldset()
          ->id('stock_group_image')
          ->title('Image')
          ->startWidget('stock_eimage_data')
          ->endWidget('stock_legend')
          );

      // Select/unselect all fields
      $form->addCheckBoxes(
          Set::checkboxes()
          ->id('attribute_check_all')
          ->options(
              array(
                'select_all' => 'All Fields',
              )
              )
          ->ajax(
              array(
                'callback' => 'tripal_megasearch_ajax_toggle_field_selection',
                'wrapper' => 'chado_search-tripal_megasearch-checkboxes-attribute_checkboxes',
                'effect' => 'fade'
              )
              )
          ->defaultValue(array('select_all'))
          ->fieldset('data_attributes')
          ->newline()
          );
      // Populate attribuet list
      $form->addCheckboxes(
          Set::checkBoxes()
          ->id('attribute_checkboxes')
          ->options($fields)
          ->fieldset('data_attributes')
          ->defaultValue(array_keys($fields))
          );
    }
  }
  return $form;
}

function tripal_megasearch_ajax_stock_get_filters(&$form_state) {
  $where = array();
  $where [] = Sql::selectFilter('stock_organism', $form_state, 'organism');
  $where [] = Sql::textFilterOnMultipleColumns('stock_name', $form_state, array('uniquename', 'name'));
  $where [] = Sql::file('stock_file', 'name', FALSE, TRUE);
  $where [] = Sql::selectFilter('stock_collection', $form_state, 'collection');
  $where [] = Sql::textFilter('stock_accession', $form_state, 'accession');
  $where [] = Sql::textFilter('stock_maternal_parent', $form_state, 'maternal_parent');
  $where [] = Sql::textFilter('stock_paternal_parent', $form_state, 'paternal_parent');
  $where [] = Sql::textFilter('stock_pedigree', $form_state, 'pedigree');
  $where [] = Sql::selectFilter('stock_country', $form_state, 'country');
  $where [] = Sql::textFilter('stock_eimage_data', $form_state, 'eimage_data');
  $where [] = Sql::textFilter('stock_legend', $form_state, 'legend');
  return $where;
}

/*
 * Refresh count
 */
function tripal_megasearch_ajax_stock_refresh_count ($form, &$form_state) {
  $where = tripal_megasearch_ajax_stock_get_filters($form_state);
  return tripal_megasearch_get_form_status($form, $form_state, $where);
}

function tripal_megasearch_ajax_stock_view_result ($form, &$form_state) {
  $where = tripal_megasearch_ajax_stock_get_filters($form_state);
  $result = $form['results'];
  $result['#markup'] = tripal_megasearch_ajax_get_table ($form, $form_state, $where);
  return $result;
}

/*
  * Create download
  */
function tripal_megasearch_ajax_stock_create_download ($form, &$form_state) {
  $where = tripal_megasearch_ajax_stock_get_filters($form_state);
  $element = $form_state->getTriggeringElement();
  $format = $element['#value'] == 'TSV' ? 'TSV' : 'CSV';
  return tripal_megasearch_ajax_get_download_file ($form, $form_state, $where, $format);
}