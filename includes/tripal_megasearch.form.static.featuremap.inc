<?php

use Drupal\chado_search\Core\Set;
use Drupal\chado_search\Core\Sql;

function tripal_megasearch_featuremap_form ($form) {
  $form_state =& $form->form_state;

  // Settings for the Filter & Attribute lists (hide if datatype not selected)
  $setttings_filter =
  Set::dynamicFieldset()
  ->id('data_filters')
  ->title('Query')
  ->dependOnId('datatype');
  $setttings_attr =
  Set::dynamicFieldset()
  ->id('data_attributes')
  ->title('Downloadable Fields')
  ->dependOnId('datatype');
  if(!$form_state->getValue('datatype') || $form_state->getValue('datatype') == '0') {
    $setttings_filter->display('none');
    $setttings_attr->display('none');
  }

  // Filter List fieldset
  $form->addDynamicFieldset($setttings_filter);

  // Attribute List fieldset
  $form->addDynamicFieldset($setttings_attr);

  if($form_state->getValue('datatype')) {
    $mview = $form_state->getValue('datatype');
    $fields = chado_search_get_mview_setting('tripal_megasearch_data_definition', $mview, 'field');
    if (chado_search_get_mview_setting('tripal_megasearch_data_definition', $mview, 'maintenance')) {
      $form->addMarkup(Set::markup()->id('maintenance')->text('<span style="color:red">Under maintenance. Data  may not be available. Please check back later.</span>')->fieldset('data_filters'));
    }
    if ($fields) {
      $form->addButton(
          Set::button()
          ->id('tsv-download')
          ->value('TSV')
          ->fieldset('data_attributes')
          ->ajax(array(
              'callback' => 'tripal_megasearch_ajax_featuremap_create_download',
              'wrapper' => 'chado_search-filter-tripal_megasearch-status-field',
              'effect' => 'fade')
              )
          ->attributes(tripal_megasearch_generate_progress_attribute())
          );
      $form->addSubmit(
          Set::submit()
          ->value('CSV')
          ->fieldset('data_attributes')
          ->ajax(array(
            'callback' => 'tripal_megasearch_ajax_featuremap_create_download',
            'wrapper' => 'chado_search-filter-tripal_megasearch-status-field',
            'effect' => 'fade')
              )
          ->attributes(tripal_megasearch_generate_progress_attribute())
          );
      $default_rows = \Drupal::state()->get('tripal_megasearch_result_rows', '10');
      if ($default_rows > 0) {
        $form->addButton(
            Set::button()
            ->id('view_results')
            ->fieldset('data_attributes')
            ->value('View')
            ->ajax(array(
              'callback' => 'tripal_megasearch_ajax_featuremap_view_result',
              'wrapper' => 'chado_search-tripal_megasearch-markup-results',
              'effect' => 'fade')
                )
            );
      }
      $form->addButton(
          Set::button()
          ->id('apply_filters')
          ->fieldset('data_filters')
          ->value('Refresh Count')
          ->ajax(array(
            'callback' => 'tripal_megasearch_ajax_featuremap_refresh_count',
            'wrapper' => 'chado_search-filter-tripal_megasearch-status-field',
            'effect' => 'fade')
              )
          );
      $form->addButton(
          Set::button()
          ->id('clear-all-values')
          ->fieldset('data_filters')
          ->value('Clear')
          ->ajax(array(
              'callback' => 'tripal_megasearch_ajax_filter_clear_values',
              'wrapper' => 'chado_search-filter-tripal_megasearch-data_filters-field',
              'effect' => 'fade')
              )
          );

      $form->addSelectFilter(
          Set::selectFilter()
          ->id('map_organism')
          ->title('Organism')
          ->table('tripal_megasearch_featuremap')
          ->column('organism')
          ->fieldset('data_filters')
          ->multiple(TRUE)
          ->searchBox(TRUE)
          ->cache(TRUE)
          ->newline()
          );
      if (tripal_megasearch_api_check(4)) {
      $form->addDynamicSelectFilter(
          Set::dynamicSelectFilter()
          ->id('map_name')
          ->title('Map Name')
          ->dependOnId('map_organism')
          ->cache('tripal_megasearch_featuremap', ['name', 'organism'])
          ->cachedOpts('name', 'organism')
          ->fieldset('data_filters')
          ->newLine()
          );
      }
      // Mapping population
      $form->addTextFilter(
          Set::textFilter()
          ->id('map_population')
          ->title('Population')
          ->fieldset('data_filters')
          ->labelWidth(140)
          ->newline()
          );
      $form->addTextFilter(
          Set::textFilter()
          ->id('map_maternal_parent')
          ->title('Maternal Parent')
          ->fieldset('data_filters')
          ->labelWidth(140)
          ->newline()
          );
      $form->addTextFilter(
          Set::textFilter()
          ->id('map_paternal_parent')
          ->title('Paternal Parent')
          ->fieldset('data_filters')
          ->labelWidth(140)
          ->newline()
          );
      $form->addFieldset(
          Set::fieldset()
          ->id('map_group_mapping')
          ->title('Mapping population')
          ->startWidget('map_population')
          ->endWidget('map_paternal_parent')
          );
      // QTL
      $form->addSelectFilter(
          Set::selectFilter()
          ->id('map_category')
          ->title('Trait Category')
          ->table('tripal_megasearch_featuremap')
          ->column('category')
          ->labelWidth(140)
          ->fieldset('data_filters')
          ->cache(TRUE)
          ->newline()
          );
      if (tripal_megasearch_api_check(4)) {
        $form->addDynamicSelectFilter(
            Set::dynamicSelectFilter()
            ->id('map_trait')
            ->title('Trait Name')
            ->fieldset('data_filters')
            ->labelWidth(140)
            ->cache('tripal_megasearch_featuremap', ['category', 'trait'])
            ->cachedOpts('trait', 'category')
            ->dependOnId('map_category')
            ->multiple(TRUE)
            ->searchBox(TRUE)
            ->newline()
            );
      }
      $form->addTextFilter(
          Set::textFilter()
          ->id('map_published_symbol')
          ->title('Published Symbol')
          ->fieldset('data_filters')
          ->labelWidth(140)
          ->newline()
          );
      $form->addTextFilter(
          Set::textFilter()
          ->id('map_qtl')
          ->title('QTL Label')
          ->fieldset('data_filters')
          ->labelWidth(140)
          ->newline()
          );
      $form->addFieldset(
          Set::fieldset()
          ->id('map_group_qtl')
          ->title('QTL')
          ->startWidget('map_category')
          ->endWidget('map_qtl')
          );
      $form->addTextFilter(
          Set::textFilter()
          ->id('map_citation')
          ->title('Citation')
          ->fieldset('data_filters')
          ->labelWidth(140)
          ->newline()
          );
      $form->addFieldset(
          Set::fieldset()
          ->id('map_group_pub')
          ->title('Publication')
          ->startWidget('map_citation')
          ->endWidget('map_citation')
          );
      $form->addTextFilter(
          Set::textFilter()
          ->id('map_num_of_lg')
          ->title('Number of LG')
          ->fieldset('data_filters')
          ->numeric(TRUE)
          ->labelWidth(140)
          ->newline()
          );
      $form->addTextFilter(
          Set::textFilter()
          ->id('map_num_of_loci')
          ->title('Number of Loci')
          ->fieldset('data_filters')
          ->numeric(TRUE)
          ->labelWidth(140)
          ->newline()
          );
      $form->addFieldset(
          Set::fieldset()
          ->id('map_group_size')
          ->title('Size')
          ->startWidget('map_num_of_lg')
          ->endWidget('map_num_of_loci')
          );

      // Select/unselect all fields
      $form->addCheckBoxes(
          Set::checkboxes()
          ->id('attribute_check_all')
          ->options(
              array(
                'select_all' => 'All Fields',
              )
              )
          ->ajax(
              array(
                'callback' => 'tripal_megasearch_ajax_toggle_field_selection',
                'wrapper' => 'chado_search-tripal_megasearch-checkboxes-attribute_checkboxes',
                'effect' => 'fade'
              )
              )
          ->defaultValue(array('select_all'))
          ->fieldset('data_attributes')
          ->newline()
          );
      // Populate attribuet list
      $form->addCheckboxes(
          Set::checkBoxes()
          ->id('attribute_checkboxes')
          ->options($fields)
          ->fieldset('data_attributes')
          ->defaultValue(array_keys($fields))
          );
    }
  }
  return $form;
}

function tripal_megasearch_ajax_featuremap_get_filters(&$form_state) {
  $where = array();
  $where [] = Sql::selectFilter('map_organism', $form_state, 'organism');
  $where [] = Sql::selectFilter('map_name', $form_state, 'name');
  $where [] = Sql::textFilter('map_population', $form_state, 'population');
  $where [] = Sql::textFilter('map_maternal_parent', $form_state, 'maternal_parent');
  $where [] = Sql::textFilter('map_paternal_parent', $form_state, 'paternal_parent');
  $where [] = Sql::selectFilter('map_category', $form_state, 'category');
  $where [] = Sql::selectFilter('map_trait', $form_state, 'trait');
  $where [] = Sql::textFilter('map_published_symbol', $form_state, 'published_symbol');
  $where [] = Sql::textFilter('map_qtl', $form_state, 'qtl');
  $where [] = Sql::textFilter('map_citation', $form_state, 'citation');
  $where [] = Sql::textFilter('map_num_of_lg', $form_state, 'num_of_lg');
  $where [] = Sql::textFilter('map_num_of_loci', $form_state, 'num_of_loci');
  return $where;
}

/*
 * Refresh count
 */
function tripal_megasearch_ajax_featuremap_refresh_count ($form, &$form_state) {
  $where = tripal_megasearch_ajax_featuremap_get_filters($form_state);
  return tripal_megasearch_get_form_status($form, $form_state, $where);
}

function tripal_megasearch_ajax_featuremap_view_result ($form, &$form_state) {
  $where = tripal_megasearch_ajax_featuremap_get_filters($form_state);
  $result = $form['results'];
  $result['#markup'] = tripal_megasearch_ajax_get_table ($form, $form_state, $where);
  return $result;
}


/*
  * Create download
  */
function tripal_megasearch_ajax_featuremap_create_download ($form, &$form_state) {
  $where = tripal_megasearch_ajax_featuremap_get_filters($form_state);
  $element = $form_state->getTriggeringElement();
  $format = $element['#value'] == 'TSV' ? 'TSV' : 'CSV';
  return tripal_megasearch_ajax_get_download_file ($form, $form_state, $where, $format);
}
