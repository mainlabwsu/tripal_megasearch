<?php

use Drupal\chado_search\Core\Set;
use Drupal\chado_search\Core\Sql;

function tripal_megasearch_gene_form ($form) {
  $form_state =& $form->form_state;

  // Settings for the Filter & Attribute lists (hide if datatype not selected)
  $setttings_filter =
  Set::dynamicFieldset()
  ->id('data_filters')
  ->title('Query')
  ->dependOnId('datatype');
  $setttings_attr =
  Set::dynamicFieldset()
  ->id('data_attributes')
  ->title('Downloadable Fields')
  ->dependOnId('datatype');
  if(!$form_state->getValue('datatype') || $form_state->getValue('datatype') == '0') {
    $setttings_filter->display('none');
    $setttings_attr->display('none');
  }

  // Filter List fieldset
  $form->addDynamicFieldset($setttings_filter);

  // Attribute List fieldset
  $form->addDynamicFieldset($setttings_attr);

  if($form_state->getValue('datatype')) {
    $mview = $form_state->getValue('datatype');
    $fields = chado_search_get_mview_setting('tripal_megasearch_data_definition', $mview, 'field');
    if (chado_search_get_mview_setting('tripal_megasearch_data_definition', $mview, 'maintenance')) {
      $form->addMarkup(Set::markup()->id('maintenance')->text('<span style="color:red">Under maintenance. Data  may not be available. Please check back later.</span>')->fieldset('data_filters'));
    }
    if ($fields) {
      $form->addButton(
          Set::button()
          ->id('tsv-download')
          ->value('TSV')
          ->fieldset('data_attributes')
          ->ajax(array(
              'callback' => 'tripal_megasearch_ajax_gene_create_download',
              'wrapper' => 'chado_search-filter-tripal_megasearch-status-field',
              'effect' => 'fade')
              )
          ->attributes(tripal_megasearch_generate_progress_attribute())
          );
      $form->addSubmit(
          Set::submit()
          ->value('CSV')
          ->fieldset('data_attributes')
          ->ajax(array(
            'callback' => 'tripal_megasearch_ajax_gene_create_download',
            'wrapper' => 'chado_search-filter-tripal_megasearch-status-field',
            'effect' => 'fade')
              )
          ->attributes(tripal_megasearch_generate_progress_attribute())
          );
      // Fasta download if available
      $fasta = chado_search_get_mview_setting('tripal_megasearch_data_definition', $mview, 'fasta');
      if ($fasta) {
        $form->addButton(
            Set::button()
            ->id('fasta_file')
            ->fieldset('data_attributes')
            ->value('FASTA')
            ->ajax(array(
              'callback' => 'tripal_megasearch_ajax_gene_create_fasta_download',
              'wrapper' => 'chado_search-filter-tripal_megasearch-status-field',
              'effect' => 'fade')
                )
            ->attributes(tripal_megasearch_generate_progress_attribute())
            );
      }
      $default_rows = \Drupal::state()->get('tripal_megasearch_result_rows', '10');
      if ($default_rows > 0) {
      $form->addButton(
          Set::button()
          ->id('view_results')
          ->fieldset('data_attributes')
          ->value('View')
          ->ajax(array(
            'callback' => 'tripal_megasearch_ajax_gene_view_result',
            'wrapper' => 'chado_search-tripal_megasearch-markup-results',
            'effect' => 'fade')
              )
          );
      }
      $sr = \Drupal::state()->get('tripal_megasearch_sequence_retrieval', ['gene' => 0, 'marker' =>0]);
      if ($sr['gene'] && \Drupal::database()->schema()->tableExists('chado.tripal_megasearch_gene') && \Drupal::database()->schema()->fieldExists('chado.tripal_megasearch_gene', 'landmark_id')) {
        $form->addSequenceRetrieval (
            Set::sequenceRetrieval()
            ->id('gene_sequence_retrieval')
            ->fieldset('data_attributes')
            );
        $form->form['data_attributes']['gene_sequence_retrieval']['#description'] = '<i>Warning: this may take hours if too many sequences are being downloaded. Please do not start two sequence retrieval jobs at the same time.</i>';
        $form->form['data_attributes']['gene_sequence_retrieval']['retrieve_button'] = [
          '#id' => 'tripal_megasearch_sequence_retrieval_button',
          '#type' => 'button',
          '#value' => 'Retrieve',
          '#ajax' => [
            'callback' => 'tripal_megasearch_ajax_gene_sequence_retrieval',
            'wrapper' => 'chado_search-filter-tripal_megasearch-status-field',
            'effect' => 'fade'
          ],
        ];
      }
      $form->addButton(
          Set::button()
          ->id('apply_filters')
          ->fieldset('data_filters')
          ->value('Refresh Count')
          ->ajax(array(
            'callback' => 'tripal_megasearch_ajax_gene_refresh_count',
            'wrapper' => 'chado_search-filter-tripal_megasearch-status-field',
            'effect' => 'fade')
              )
          ->attributes(tripal_megasearch_generate_waiting_attribute())
          );
      $form->addButton(
          Set::button()
          ->id('clear-all-values')
          ->fieldset('data_filters')
          ->value('Clear')
          ->ajax(array(
              'callback' => 'tripal_megasearch_ajax_filter_clear_values',
              'wrapper' => 'chado_search-filter-tripal_megasearch-data_filters-field',
              'effect' => 'fade')
              )
          );
      // Sequence Type
      $form->addSelectFilter(
          Set::selectFilter()
          ->id('gene_type')
          ->title('Sequence Type')
          ->table('tripal_megasearch_gene')
          ->column('type')
          ->labelWidth(150)
          ->fieldset('data_filters')
          ->cache(TRUE)
          ->newline()
          );

      // Genome
      $form->addSelectFilter(
          Set::selectFilter()
          ->id('gene_genome')
          ->title('Genome Name')
          ->table('tripal_megasearch_gene')
          ->column('analysis')
          ->condition("analysis_type='whole_genome'")
          ->labelWidth(150)
          ->fieldset('data_filters')
          ->multiple(TRUE)
          ->cache(TRUE)
          ->newline()
          );
      $form->addDynamicSelectFilter(
          Set::dynamicSelectFilter()
          ->id('gene_glandmark')
          ->title('Chromosome/Scaffold')
          ->dependOnId('gene_genome')
          ->callback('tripal_megasearch_ajax_gene_populate_landmark')
          ->labelWidth(150)
          ->fieldset('data_filters')
          ->cache('tripal_megasearch_gene', array('landmark', 'analysis', 'genome'))
          ->newLine()
          );

      $form->addTextFilter(
          Set::textFilter()
          ->id('gene_gstart')
          ->title('Start')
          ->numeric(TRUE)
          ->fieldset('data_filters')
          ->labelWidth(150)
          ->newline()
          );
      $form->addTextFilter(
          Set::textFilter()
          ->id('gene_gstop')
          ->title('Stop')
          ->numeric(TRUE)
          ->fieldset('data_filters')
          ->labelWidth(150)
          ->newline()
          );
      $form->addFieldset(
          Set::fieldset()
          ->id('gene_group_genome')
          ->title('Genome')
          ->startWidget('gene_genome')
          ->endWidget('gene_gstop')
          );

      // Transcriptome
      $form->addSelectFilter(
          Set::selectFilter()
          ->id('gene_transcriptome')
          ->title('Transcriptome/Dataset')
          ->table('tripal_megasearch_gene')
          ->column('analysis')
          ->condition("analysis_type <> 'whole_genome'")
          ->labelWidth(150)
          ->optGroupByPattern(array('Transcriptome' => 'RefTrans'))
          ->fieldset('data_filters')
          ->cache(TRUE)
          ->newline()
          );
      $form->addDynamicSelectFilter(
          Set::dynamicSelectFilter()
          ->id('gene_torganism')
          ->title('Organism')
          ->dependOnId('gene_transcriptome')
          ->callback('tripal_megasearch_ajax_gene_populate_torganism')
          ->fieldset('data_filters')
          ->labelWidth(150)
          ->newline()
          );
      $form->addDynamicSelectFilter(
          Set::dynamicSelectFilter()
          ->id('gene_aligned_genome')
          ->title('Genome')
          ->dependOnId('gene_transcriptome')
          ->callback('tripal_megasearch_ajax_gene_populate_tgenome')
          ->labelWidth(150)
          ->fieldset('data_filters')
          ->newline()
          );
      $form->addDynamicSelectFilter(
          Set::dynamicSelectFilter()
          ->id('gene_tlandmark')
          ->title('Chromosome/Scaffold')
          ->dependOnId('gene_transcriptome')
          ->callback('tripal_megasearch_ajax_gene_populate_tlandmark')
          ->labelWidth(150)
          ->fieldset('data_filters')
          ->newLine()
          );

      $form->addTextFilter(
          Set::textFilter()
          ->id('gene_tstart')
          ->title('Start')
          ->numeric(TRUE)
          ->fieldset('data_filters')
          ->labelWidth(150)
          ->newline()
          );
      $form->addTextFilter(
          Set::textFilter()
          ->id('gene_tstop')
          ->title('Stop')
          ->numeric(TRUE)
          ->fieldset('data_filters')
          ->labelWidth(150)
          ->newline()
          );
      $form->addFieldset(
          Set::fieldset()
          ->id('gene_group_transcriptome')
          ->title('Transcriptome/Other Dataset')
          ->startWidget('gene_transcriptome')
          ->endWidget('gene_tstop')
          );

      // Gene/Transcript Name
      $form->addTextFilter(
          Set::textFilter()
          ->id('gene_name')
          ->title('Name')
          ->labelWidth(150)
          ->fieldset('data_filters')
          );
      $form->addFile(
          Set::file()
          ->id('gene_file')
          ->title("File Upload")
          ->fieldset('data_filters')
          ->description("Provide names in a file. Separate each name by a new line.")
          );
      $form->addCheckBoxes(
          Set::checkboxes()
          ->id('gene_associated_mrna')
          ->options(['associated_mrna' => 'Also search associated mRNA/gene if the name in uploaded file matches a gene/mRNA'])
          ->fieldset('data_filters')
          );
      $form->addFieldset(
          Set::fieldset()
          ->id('gene_group_name')
          ->title('Gene/Transcript name')
          ->startWidget('gene_name')
          ->endWidget('gene_associated_mrna')
          );

      // Functional Annotation
      $form->addTextFilter(
          Set::textFilter()
          ->id('gene_all')
          ->title('All')
          ->fieldset('data_filters')
          ->labelWidth(150)
          ->newline()
          );
      $form->addTextFilter(
          Set::textFilter()
          ->id('gene_blast')
          ->title('BLAST')
          ->fieldset('data_filters')
          ->labelWidth(150)
          ->newline()
          );
      /*
      $form->addTextFilter(
          Set::textFilter()
          ->id('gene_kegg')
          ->title('KEGG')
          ->fieldset('data_filters')
          ->labelWidth(150)
          ->newline()
          );*/
      $form->addTextFilter(
          Set::textFilter()
          ->id('gene_interpro')
          ->title('InterPro')
          ->fieldset('data_filters')
          ->labelWidth(150)
          ->newline()
          );
      $form->addTextFilter(
          Set::textFilter()
          ->id('gene_go')
          ->title('GO Term')
          ->fieldset('data_filters')
          ->labelWidth(150)
          ->newline()
          );
      $form->addTextFilter(
          Set::textFilter()
          ->id('gene_genbank')
          ->title('GenBank Keyword')
          ->fieldset('data_filters')
          ->labelWidth(150)
          ->newline()
          );
      $form->addFieldset(
          Set::fieldset()
          ->id('gene_group_annotation')
          ->title('Functional Annotation')
          ->startWidget('gene_all')
          ->endWidget('gene_genbank')
          );

      // Select/unselect all fields
      $form->addCheckBoxes(
          Set::checkboxes()
          ->id('attribute_check_all')
          ->options(
              array(
                'select_all' => 'All Fields',
              )
              )
          ->ajax(
              array(
                'callback' => 'tripal_megasearch_ajax_toggle_field_selection',
                'wrapper' => 'chado_search-tripal_megasearch-checkboxes-attribute_checkboxes',
                'effect' => 'fade'
              )
              )
          ->defaultValue(array('select_all'))
          ->fieldset('data_attributes')
          ->newline()
          );
      // Populate attribuet list
      $form->addCheckboxes(
          Set::checkBoxes()
          ->id('attribute_checkboxes')
          ->options($fields)
          ->fieldset('data_attributes')
          ->defaultValue(array_keys($fields))
          );
    }
  }
  return $form;
}

function tripal_megasearch_ajax_gene_get_filters(&$form_state) {
  $search_assoc_mrna = $form_state->getValue('gene_associated_mrna')['associated_mrna'];
  
  $where = array();
  $where [] = Sql::selectFilter('gene_genome', $form_state, 'analysis');
  $where [] = Sql::selectFilter('gene_glandmark', $form_state, 'landmark');
  $where [] = Sql::textFilter('gene_gstart', $form_state, 'fmin');
  $where [] = Sql::textFilter('gene_gstop', $form_state, 'fmax');
  $where [] = Sql::selectFilter('gene_transcriptome', $form_state, 'analysis');
  $where [] = Sql::selectFilter('gene_torganism', $form_state, 'organism');
  $where [] = Sql::selectFilter('gene_aligned_genome', $form_state, 'genome');
  $where [] = Sql::selectFilter('gene_tlandmark', $form_state, 'landmark');
  $where [] = Sql::textFilter('gene_tstart', $form_state, 'fmin');
  $where [] = Sql::textFilter('gene_tstop', $form_state, 'fmax');
  $where [] = Sql::textFilterOnMultipleColumns('gene_name', $form_state, array('name', 'uniquename'));
  if ($search_assoc_mrna != 0) {
    // Gather all feature_id to be used
    $gnames = [];
    $file = isset($_FILES['files']['tmp_name']['gene_file']) ? $_FILES['files']['tmp_name']['gene_file'] : NULL;
    if ($file) {
      $handle = fopen($file, 'r');
      $counter = 1;
      $uplimit = \Drupal::state()->get('chado_search_upload_limit', '0');  // limit of lines to read
      $counter = 0;
      while ($line = fgets($handle)) {
        $name = trim($line);
        $name = str_replace("'", "''", $name); // escape the single quote
        $gnames[] = $name;
        if ($uplimit != 0 && $counter > $uplimit) {
          break;
        }
        $counter ++;
      }
      fclose($handle);
    }
    // Search only if there is a list of gene names
    if (count($gnames) > 0) {
      // Construct SQL
      $sql_names = '';
      foreach ($gnames AS $g) {
        $sql_names .= "'$g', ";
      }
      $sql_names = trim($sql_names, ', ');
      $sql = 'SELECT feature_id, (SELECT name FROM chado.cvterm WHERE cvterm_id = type_id)  AS type FROM chado.feature WHERE name IN (' . $sql_names . ') OR uniquename IN (' . $sql_names . ')';
      $results = \Drupal::database()->query($sql);
      // Find matched features. Populate feature_id for genes/mRNA and construct query for associated mRNA/genes
      $feature_ids = [];
      $assoc_mrna_ids = '';
      $assoc_gene_ids = '';
      while ($feature = $results->fetchObject()) {
        $feature_ids [] = $feature->feature_id;          
        if ($feature->type == 'gene') {
          $assoc_mrna_ids .= $feature->feature_id . ', ';
        }
        else if ($feature->type == 'mRNA') {
          $assoc_gene_ids .= $feature->feature_id . ', ';
        }
      }
      $assoc_mrna_ids = trim($assoc_mrna_ids, ', ');
      $assoc_gene_ids = trim($assoc_gene_ids, ', ');
      // Find associated mRNAs
      if ($assoc_mrna_ids != '') {
        $sql_assoc_m = "SELECT subject_id FROM chado.feature_relationship WHERE type_id = (SELECT cvterm_id FROM chado.cvterm WHERE name = 'part_of' AND cv_id = (SELECT cv_id FROM chado.cv WHERE name = 'sequence')) AND object_id IN (" . $assoc_mrna_ids . ')';
        $results = \Drupal::database()->query($sql_assoc_m);
        while($assoc_m = $results->fetchObject()) {
          $feature_ids[] = $assoc_m->subject_id;
        }
      }
      // Find associated genes
      if ($assoc_gene_ids != '') {
        $sql_assoc_g = "SELECT object_id FROM chado.feature_relationship WHERE type_id = (SELECT cvterm_id FROM chado.cvterm WHERE name = 'part_of' AND cv_id = (SELECT cv_id FROM chado.cv WHERE name = 'sequence')) AND subject_id IN (" . $assoc_gene_ids . ')';
        $results = \Drupal::database()->query($sql_assoc_g);
        while($assoc_g = $results->fetchObject()) {
          $feature_ids[] = $assoc_g->object_id;
        }
      }
      
      // Finally, construct WHERE clause for the search
      if (count($feature_ids) > 0) {
        $sql_fid = '';
        foreach($feature_ids AS $fid) {
          $sql_fid .= $fid . ', ';
        }
        $where[] = '(feature_id IN (' . trim($sql_fid, ', ') . '))';
      }
    }
  } else {
    $where [] = Sql::fileOnMultipleColumns('gene_file', array('name', 'uniquename'), TRUE);
  }
  $where [] = Sql::selectFilter('gene_type', $form_state, 'type');
  $where [] = Sql::textFilterOnMultipleColumns('gene_all', $form_state, array('blast', 'interpro', 'go', 'genbank'));
  $where [] = Sql::textFilter('gene_blast', $form_state, 'blast');
  //$where [] = Sql::textFilter('gene_kegg', $form_state, 'kegg');
  $where [] = Sql::textFilter('gene_interpro', $form_state, 'interpro');
  $where [] = Sql::textFilter('gene_go', $form_state, 'go');
  $where [] = Sql::textFilter('gene_genbank', $form_state, 'genbank');
  return $where;
}

/*
 * Refresh count
 */
function tripal_megasearch_ajax_gene_refresh_count ($form, &$form_state) {
  $where = tripal_megasearch_ajax_gene_get_filters($form_state);
  return tripal_megasearch_get_form_status($form, $form_state, $where);
}

function tripal_megasearch_ajax_gene_view_result ($form, &$form_state) {
  $where = tripal_megasearch_ajax_gene_get_filters($form_state);
  $result = $form['results'];
  $result['#markup'] = tripal_megasearch_ajax_get_table ($form, $form_state, $where);
  return $result;
}

/*
  * Create download
  */
function tripal_megasearch_ajax_gene_create_download ($form, &$form_state) {
  $where = tripal_megasearch_ajax_gene_get_filters($form_state);
  $element = $form_state->getTriggeringElement();
  $format = $element['#value'] == 'TSV' ? 'TSV' : 'CSV';
  return tripal_megasearch_ajax_get_download_file ($form, $form_state, $where, $format);
}

/*
 * Create FASTA download
 */
function tripal_megasearch_ajax_gene_create_fasta_download ($form, &$form_state) {
  $where = tripal_megasearch_ajax_gene_get_filters($form_state);
  return tripal_megasearch_ajax_get_fasta_file ($form, $form_state, $where);
}

/*
 * Sequence Retrieval
 */
function tripal_megasearch_ajax_gene_sequence_retrieval ($form, &$form_state) {
  $values = $form_state->getValues();
  $where = tripal_megasearch_ajax_gene_get_filters($form_state);
  $settings = [
    'feature_id' => 'feature_id',
    'name' => 'uniquename',
    'landmark_id' => 'landmark_id',
    'fmin' => 'fmin',
    'fmax' => 'fmax',
    'strand' => NULL,
    'distinct' => 'feature_id,landmark_id,fmin,fmax',
    'fmin_coordinate' => 1,
    'upstream' => $values['sr_upstream'] ? $values['sr_upstream'] : 0,
    'downstream' => $values['sr_downstream'] ? $values['sr_downstream'] : 0,
  ];
  return tripal_megasearch_ajax_get_sequence_retrieval_file ($form, $form_state, $where, $settings);
}

function tripal_megasearch_ajax_gene_populate_landmark($val) {
  $table = \Drupal::database()->schema()->tableExists('cache_tripal_megasearch_gene_glandmark') ? 'cache_tripal_megasearch_gene_glandmark' : 'tripal_megasearch_gene';
  $sql = "SELECT distinct landmark FROM {$table} WHERE analysis IN (:analysis[]) ORDER BY landmark";
  return chado_search_bind_dynamic_select(array(':analysis[]' => $val), 'landmark', $sql);
}

function tripal_megasearch_ajax_gene_populate_tgenome($val) {
  $table = \Drupal::database()->schema()->fieldExists('cache_tripal_megasearch_gene_glandmark', 'genome') ? 'cache_tripal_megasearch_gene_glandmark' : 'tripal_megasearch_gene';
  $sql = "SELECT distinct genome FROM {$table} WHERE analysis IN (:analysis) ORDER BY genome";
  return chado_search_bind_dynamic_select(array(':analysis' => $val), 'genome', $sql);
}

function tripal_megasearch_ajax_gene_populate_torganism($val) {
  $table = 'tripal_megasearch_gene';
  $sql = "SELECT distinct organism FROM {$table} WHERE analysis = (:analysis) ORDER BY organism";
  return chado_search_bind_dynamic_select(array(':analysis' => $val), 'organism', $sql);
}

function tripal_megasearch_ajax_gene_populate_tlandmark($val) {
  $table = \Drupal::database()->schema()->tableExists('cache_tripal_megasearch_gene_glandmark') ? 'cache_tripal_megasearch_gene_glandmark' : 'tripal_megasearch_gene';
  $sql = "SELECT distinct landmark FROM {$table} WHERE analysis IN (:analysis) ORDER BY landmark";
  return chado_search_bind_dynamic_select(array(':analysis' => $val), 'landmark', $sql);
}