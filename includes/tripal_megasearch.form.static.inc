<?php

function tripal_megasearch_query_form_static($form) {

  $form_state =& $form->form_state;

  // Get the form callback function from MView settings
  $callback = '';
  if($form_state->getValue('datatype')) {
    $mview = $form_state->getValue('datatype');
    $callback = chado_search_get_mview_setting('tripal_megasearch_data_definition', $mview, 'form');
  }

  // If callback exists, pass $form to the callback for adding more filters
  if(function_exists($callback)) {
    $form = $callback($form);
  }
  // If callback not exists, use the default dynamic form
  else {
    \Drupal::moduleHandler()->loadInclude('tripal_megasearch', 'inc', 'includes/tripal_megasearch.form.dynamic');
    $form = tripal_megasearch_query_form_dynamic($form);
  }

  return $form;
}