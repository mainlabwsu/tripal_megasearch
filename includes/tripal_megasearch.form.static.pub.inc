<?php

use Drupal\chado_search\Core\Set;
use Drupal\chado_search\Core\Sql;

function tripal_megasearch_pub_form ($form) {

  $form_state =& $form->form_state;

  // Settings for the Filter & Attribute lists (hide if datatype not selected)
  $setttings_filter =
  Set::dynamicFieldset()
  ->id('data_filters')
  ->title('Query')
  ->dependOnId('datatype');
  $setttings_attr =
  Set::dynamicFieldset()
  ->id('data_attributes')
  ->title('Downloadable Fields')
  ->dependOnId('datatype');
  if(!$form_state->getValue('datatype') || $form_state->getValue('datatype') == '0') {
    $setttings_filter->display('none');
    $setttings_attr->display('none');
  }

  // Filter List fieldset
  $form->addDynamicFieldset($setttings_filter);

  // Attribute List fieldset
  $form->addDynamicFieldset($setttings_attr);

  if($form_state->getValue('datatype')) {
    $mview = $form_state->getValue('datatype');
    $fields = chado_search_get_mview_setting('tripal_megasearch_data_definition', $mview, 'field');
    if (chado_search_get_mview_setting('tripal_megasearch_data_definition', $mview, 'maintenance')) {
      $form->addMarkup(Set::markup()->id('maintenance')->text('<span style="color:red">Under maintenance. Data  may not be available. Please check back later.</span>')->fieldset('data_filters'));
    }
    if ($fields) {
      $form->addButton(
          Set::button()
          ->id('tsv-download')
          ->value('TSV')
          ->fieldset('data_attributes')
          ->ajax(array(
              'callback' => 'tripal_megasearch_ajax_pub_create_download',
              'wrapper' => 'chado_search-filter-tripal_megasearch-status-field',
              'effect' => 'fade')
              )
          ->attributes(tripal_megasearch_generate_progress_attribute())
          );
      $form->addSubmit(
          Set::submit()
          ->value('CSV')
          ->fieldset('data_attributes')
          ->ajax(array(
            'callback' => 'tripal_megasearch_ajax_pub_create_download',
            'wrapper' => 'chado_search-filter-tripal_megasearch-status-field',
            'effect' => 'fade')
              )
          ->attributes(tripal_megasearch_generate_progress_attribute())
          );
      $default_rows = \Drupal::state()->get('tripal_megasearch_result_rows', '10');
      if ($default_rows > 0) {
        $form->addButton(
          Set::button()
          ->id('view_results')
          ->fieldset('data_attributes')
          ->value('View')
          ->ajax(array(
            'callback' => 'tripal_megasearch_ajax_pub_view_result',
            'wrapper' => 'chado_search-tripal_megasearch-markup-results',
            'effect' => 'fade')
              )
          );
      }
      $form->addButton(
          Set::button()
          ->id('apply_filters')
          ->fieldset('data_filters')
          ->value('Refresh Count')
          ->ajax(array(
            'callback' => 'tripal_megasearch_ajax_pub_refresh_count',
            'wrapper' => 'chado_search-filter-tripal_megasearch-status-field',
            'effect' => 'fade')
              )
          );
      $form->addButton(
          Set::button()
          ->id('clear-all-values')
          ->fieldset('data_filters')
          ->value('Clear')
          ->ajax(array(
              'callback' => 'tripal_megasearch_ajax_filter_clear_values',
              'wrapper' => 'chado_search-filter-tripal_megasearch-data_filters-field',
              'effect' => 'fade')
              )
          );

      // Pub Type
      $form->addSelectFilter(
          Set::selectFilter()
          ->id('pub_type')
          ->title('Publication Type')
          ->table('tripal_megasearch_pub')
          ->column('type')
          ->fieldset('data_filters')
          ->labelWidth(130)
          ->cache(TRUE)
          ->newline()
          );
      $ftypes = chado_search_get_field_types($mview, $fields);
      // Populate filter list
      $all_fields = $fields;
      unset ($fields['type']);
      $form->addRepeatableText(
          Set::repeatableText()
          ->id('conditions')
          ->items($fields)
          ->itemTypes($ftypes)
          ->fieldset('data_filters')
          ->maxlength(256)
          //->autoCompletePath('/chado_search_autocomplete/' . $mview)
          );

      // Select/unselect all fields
      $form->addCheckBoxes(
          Set::checkboxes()
          ->id('attribute_check_all')
          ->options(
              array(
                'select_all' => 'All Fields',
              )
              )
          ->ajax(
              array(
                'callback' => 'tripal_megasearch_ajax_toggle_field_selection',
                'wrapper' => 'chado_search-tripal_megasearch-checkboxes-attribute_checkboxes',
                'effect' => 'fade'
              )
              )
          ->defaultValue(array('select_all'))
          ->fieldset('data_attributes')
          ->newline()
          );
      // Populate attribuet list
      $default = array_keys($all_fields);
      $key = array_search('abstract', $default);
      unset($default[$key]);
      $form->addCheckboxes(
          Set::checkBoxes()
          ->id('attribute_checkboxes')
          ->options($all_fields)
          ->fieldset('data_attributes')
          ->defaultValue($default)
          );
    }
  }
  return $form;
}

function tripal_megasearch_ajax_pub_get_filters(&$form_state) {
  $where = array();
  $where [] = Sql::selectFilter('pub_type', $form_state, 'type');
  $where [] = Sql::repeatableText('conditions', $form_state);
  return $where;
}

/*
 * Refresh count
 */
function tripal_megasearch_ajax_pub_refresh_count ($form, &$form_state) {
  $where = tripal_megasearch_ajax_pub_get_filters($form_state);
  return tripal_megasearch_get_form_status($form, $form_state, $where);
}

function tripal_megasearch_ajax_pub_view_result ($form, &$form_state) {
  $where = tripal_megasearch_ajax_pub_get_filters($form_state);
  $result = $form['results'];
  $result['#markup'] = tripal_megasearch_ajax_get_table ($form, $form_state, $where);
  return $result;
}

/*
 * Create download
 */
function tripal_megasearch_ajax_pub_create_download ($form, &$form_state) {
  $where = tripal_megasearch_ajax_pub_get_filters($form_state);
  $element = $form_state->getTriggeringElement();
  $format = $element['#value'] == 'TSV' ? 'TSV' : 'CSV';
  return tripal_megasearch_ajax_get_download_file ($form, $form_state, $where, $format);
}
