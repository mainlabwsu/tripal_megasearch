<?php

use Drupal\chado_search\Core\Set;
use Drupal\chado_search\Core\Sql;

function tripal_megasearch_contact_form ($form) {
  $form_state =& $form->form_state;

  // Settings for the Filter & Attribute lists (hide if datatype not selected)
  $setttings_filter =
  Set::dynamicFieldset()
  ->id('data_filters')
  ->title('Query')
  ->dependOnId('datatype');
  $setttings_attr =
  Set::dynamicFieldset()
  ->id('data_attributes')
  ->title('Downloadable Fields')
  ->dependOnId('datatype');
  if(!$form_state->getValue('datatype') || $form_state->getValue('datatype') == '0') {
    $setttings_filter->display('none');
    $setttings_attr->display('none');
  }

  // Filter List fieldset
  $form->addDynamicFieldset($setttings_filter);

  // Attribute List fieldset
  $form->addDynamicFieldset($setttings_attr);

  if($form_state->getValue('datatype')) {
    $mview = $form_state->getValue('datatype');
    $fields = chado_search_get_mview_setting('tripal_megasearch_data_definition', $mview, 'field');
    if (chado_search_get_mview_setting('tripal_megasearch_data_definition', $mview, 'maintenance')) {
      $form->addMarkup(Set::markup()->id('maintenance')->text('<span style="color:red">Under maintenance. Data  may not be available. Please check back later.</span>')->fieldset('data_filters'));
    }
    if ($fields) {
        $form->addButton(
            Set::button()
            ->id('tsv-download')
            ->value('TSV')
            ->fieldset('data_attributes')
            ->ajax(array(
                'callback' => 'tripal_megasearch_ajax_contact_create_download',
                'wrapper' => 'chado_search-filter-tripal_megasearch-status-field',
                'effect' => 'fade')
                )
            ->attributes(tripal_megasearch_generate_progress_attribute())
            );
      $form->addSubmit(
          Set::submit()
          ->value('CSV')
          ->fieldset('data_attributes')
          ->ajax(array(
            'callback' => 'tripal_megasearch_ajax_contact_create_download',
            'wrapper' => 'chado_search-filter-tripal_megasearch-status-field',
            'effect' => 'fade')
              )
          ->attributes(tripal_megasearch_generate_progress_attribute())
          );
      $default_rows = \Drupal::state()->get('tripal_megasearch_result_rows', '10');
      if ($default_rows > 0) {
        $form->addButton(
            Set::button()
            ->id('view_results')
            ->fieldset('data_attributes')
            ->value('View')
            ->ajax(array(
              'callback' => 'tripal_megasearch_ajax_contact_view_result',
              'wrapper' => 'chado_search-tripal_megasearch-markup-results',
              'effect' => 'fade')
                )
            );
      }
      $form->addButton(
          Set::button()
          ->id('apply_filters')
          ->fieldset('data_filters')
          ->value('Refresh Count')
          ->ajax(array(
            'callback' => 'tripal_megasearch_ajax_contact_refresh_count',
            'wrapper' => 'chado_search-filter-tripal_megasearch-status-field',
            'effect' => 'fade')
              )
          );
      $form->addButton(
          Set::button()
          ->id('clear-all-values')
          ->newline()
          ->fieldset('data_filters')
          ->value('Clear')
          ->ajax(array(
              'callback' => 'tripal_megasearch_ajax_filter_clear_values',
              'wrapper' => 'chado_search-filter-tripal_megasearch-data_filters-field',
              'effect' => 'fade')
              )
          );
      $form->addTextFilter(
          Set::textFilter()
          ->id('contact_lname')
          ->title('Last Name')
          ->labelWidth(140)
          ->fieldset('data_filters')
          ->newline()
          );
      $form->addTextFilter(
          Set::textFilter()
          ->id('contact_fname')
          ->title('First Name')
          ->labelWidth(140)
          ->fieldset('data_filters')
          ->newline()
          );
      $form->addTextFilter(
          Set::textFilter()
          ->id('contact_institution')
          ->title('Institution')
          ->labelWidth(140)
          ->fieldset('data_filters')
          ->newline()
          );
      $form->addSelectFilter(
          Set::selectFilter()
          ->id('contact_country')
          ->title('Country')
          ->table('tripal_megasearch_contact')
          ->column('country')
          ->labelWidth(140)
          ->fieldset('data_filters')
          ->cache(TRUE)
          ->newline()
          );
      $form->addTextFilter(
          Set::textFilter()
          ->id('contact_keywords')
          ->title('Research Interests')
          ->labelWidth(140)
          ->fieldset('data_filters')
          ->newline()
          );
      $form->addSelectFilter(
          Set::selectFilter()
          ->id('contact_type')
          ->title('Type')
          ->table('tripal_megasearch_contact')
          ->column('type')
          ->labelWidth(140)
          ->fieldset('data_filters')
          ->cache(TRUE)
          ->newline()
          );

      // Select/unselect all fields
      $form->addCheckBoxes(
          Set::checkboxes()
          ->id('attribute_check_all')
          ->options(
              array(
                'select_all' => 'All Fields',
              )
              )
          ->ajax(
              array(
                'callback' => 'tripal_megasearch_ajax_toggle_field_selection',
                'wrapper' => 'chado_search-tripal_megasearch-checkboxes-attribute_checkboxes',
                'effect' => 'fade'
              )
              )
          ->defaultValue(array('select_all'))
          ->fieldset('data_attributes')
          ->newline()
          );
      // Populate attribuet list
      $form->addCheckboxes(
          Set::checkBoxes()
          ->id('attribute_checkboxes')
          ->options($fields)
          ->fieldset('data_attributes')
          ->defaultValue(array_keys($fields))
          );
    }
  }
  return $form;
}

function tripal_megasearch_ajax_contact_get_filters(&$form_state) {
  $where = array();
  $where [] = Sql::textFilter('contact_lname', $form_state, 'lname');
  $where [] = Sql::textFilter('contact_fname', $form_state, 'fname');
  $where [] = Sql::textFilter('contact_institution', $form_state, 'institution');
  $where [] = Sql::selectFilter('contact_country', $form_state, 'country');
  $where [] = Sql::textFilter('contact_keywords', $form_state, 'research_interest');
  $where [] = Sql::selectFilter('contact_type', $form_state, 'type');
  return $where;
}

/*
 * Refresh count
 */
function tripal_megasearch_ajax_contact_refresh_count ($form, &$form_state) {
  $where = tripal_megasearch_ajax_contact_get_filters($form_state);
  return tripal_megasearch_get_form_status($form, $form_state, $where);
}

function tripal_megasearch_ajax_contact_view_result ($form, &$form_state) {
  $where = tripal_megasearch_ajax_contact_get_filters($form_state);
  $result = $form['results'];
  $result['#markup'] = tripal_megasearch_ajax_get_table ($form, $form_state, $where);
  return $result;
}

/*
  * Create download
  */
function tripal_megasearch_ajax_contact_create_download ($form, &$form_state) {
  $where = tripal_megasearch_ajax_contact_get_filters($form_state);
  $element = $form_state->getTriggeringElement();
  $format = $element['#value'] == 'TSV' ? 'TSV' : 'CSV';
  return tripal_megasearch_ajax_get_download_file ($form, $form_state, $where, $format);
}