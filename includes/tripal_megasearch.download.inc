<?php

use Drupal\Core\Render\Markup;

use Drupal\chado_search\Core\SessionVar;
use Drupal\chado_search\Core\Sql;
use Drupal\chado_search\Result\Download;

/*
 * Create download
 */
function tripal_megasearch_ajax_create_download ($form, &$form_state) {
  $where = array();
  $where [] = Sql::repeatableText('conditions', $form_state);
  $element = $form_state->getTriggeringElement();
  $format = $element['#value'] == 'TSV' ? 'TSV' : 'CSV';
  return tripal_megasearch_ajax_get_download_file ($form, $form_state, $where, $format);
}

/*
 * Apply filters and generate file
 */
function tripal_megasearch_ajax_get_download_file ($form, &$form_state, $conditions = array(), $format = 'CSV') {
  $limit = \Drupal::state()->get('tripal_megasearch_download_limit', '200000');
  $status = $form['status'];
  $values = $form_state->getValues();
  $mview = $values['datatype'];

  $where = '';
  // Remove NULL filters
  $valid_cond = array();
  foreach ($conditions AS $cond) {
    if ($cond && trim($cond)) {
      $valid_cond [] = $cond;
    }
  }
  $num_cond = count($valid_cond);
  if ($num_cond > 0) {
    $where = ' WHERE ';
  }
  $index = 0;
  foreach ($valid_cond AS $cond) {
    $where .= $cond . ' ';
    if ($index < $num_cond - 1) {
      $where .= ' AND ';
    }
    $index ++;
  }
  // Gather selected fields
  $fields = $form_state->getValue('attribute_checkboxes');
  $selected_cols = '';
  foreach ($fields AS $col) {
    if ($col) {
      $selected_cols .= $col . ',';
    }
  }
  $selected_cols = trim($selected_cols, ',');
  
  $checkbox_filter = chado_search_get_mview_setting('tripal_megasearch_data_definition', $mview, 'checkbox_filter');
  if ($checkbox_filter) {
      $selected_cols .= ',' . $checkbox_filter;
  }
  
  $sql ="SELECT DISTINCT $selected_cols FROM {" . $mview . "}" . $where;
  SessionVar::setSessionVar('tripal_megasearch','download', $sql);
  $sql_total = "SELECT count(*) FROM {" . $mview . "}";
  // if we only want to count unique value of a certain column:
  $count_col = chado_search_get_mview_setting('tripal_megasearch_data_definition', $mview, 'count');
  if ($count_col) {
    $sql_total = "SELECT count(DISTINCT $count_col) FROM {" . $mview . "}";
  }
  $total = chado_search_query($sql_total . $where, array(), 'chado')->fetchField();
  
  if ($checkbox_filter) {
      $data = SessionVar::getSessionVar('tripal_megasearch','checkbox-filter-data');
      $count_data = is_array($data) ? count($data) : 0;
      if ($count_data > 0 && $count_data <= $limit) {
          $total = $count_data;
      }
  }
  
  // Make sure there is at least one result
  if ($total == 0) {
    $status['#markup'] = Markup::create('<font class="tripal_megasearch-error" color="red">No result. Please adjust the query and try again.</font>' . '<script>jQuery(\'.chado_search-tripal_megasearch-waiting-box\').hide();</script>');
    return $status;
  }
  else if ($limit && $total > $limit) {
    $status['#markup'] = Markup::create('<strong>' . number_format($total) . '</strong> records. ' . '<font class="tripal_megasearch-error" color="red">The results exceed the limit of ' . number_format($limit) . ' records. Please add some filters and try again. </font>' . '<script>jQuery(\'.chado_search-tripal_megasearch-waiting-box\').hide();</script>');
    return $status;
  }
  else {
    SessionVar::setSessionVar('tripal_megasearch','total-items', $total);
  }
  // Limit download attributes
  $headers = chado_search_get_mview_setting('tripal_megasearch_data_definition', $mview, 'field');
  $checkboxes = $values['attribute_checkboxes'];
  foreach ($checkboxes AS $key => $val) {
    if ($val == '0') {
      unset($headers[$key]);
    }
  }
  //Make sure at least one attribute is selected
  if (count($headers) == 0) {
    $status['#markup'] = Markup::create('<font class="tripal_megasearch-error" color="red">Nothing to download. Please select at least one field.</font>' . '<script>jQuery(\'.chado_search-tripal_megasearch-waiting-box\').hide();</script>');
    return $status;
  }

  // If all checks passed, create the download file
  $dl = new Download('tripal_megasearch', 'tripal_megasearch', FALSE, $format);
  $results = $dl->createDownload($headers);
  $name = chado_search_get_mview_setting('tripal_megasearch_data_definition', $mview, 'name');
  $message = '<strong>' . number_format($total) . ' ' . $name . '</strong>.  <i>Note: actual rows in downloaded file depend on the selected fields.<i>';
  $status['#markup'] = Markup::create($message . '<script>jQuery(\'.chado_search-tripal_megasearch-waiting-box\').hide();jQuery(\'.is-invalid\').removeClass(\'form-control\');jQuery(\'.is-invalid\').removeClass(\'is-invalid\');</script>' . '<script type="text/javascript">window.location = "' . $results['path'] . '"</script>');
  return $status;
}
