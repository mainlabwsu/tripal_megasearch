<?php

use Drupal\chado_search\Core\Set;
use Drupal\chado_search\Core\Sql;

function tripal_megasearch_qtl_form ($form) {
  $form_state =& $form->form_state;

  // Settings for the Filter & Attribute lists (hide if datatype not selected)
  $setttings_filter =
  Set::dynamicFieldset()
  ->id('data_filters')
  ->title('Query')
  ->dependOnId('datatype');
  $setttings_attr =
  Set::dynamicFieldset()
  ->id('data_attributes')
  ->title('Downloadable Fields')
  ->dependOnId('datatype');
  if(!$form_state->getValues('datatype') || $form_state->getValue('datatype') == '0') {
    $setttings_filter->display('none');
    $setttings_attr->display('none');
  }

  // Filter List fieldset
  $form->addDynamicFieldset($setttings_filter);

  // Attribute List fieldset
  $form->addDynamicFieldset($setttings_attr);

  if($form_state->getValue('datatype')) {
    $mview = $form_state->getValue('datatype');
    $fields = chado_search_get_mview_setting('tripal_megasearch_data_definition', $mview, 'field');
    if (chado_search_get_mview_setting('tripal_megasearch_data_definition', $mview, 'maintenance')) {
      $form->addMarkup(Set::markup()->id('maintenance')->text('<span style="color:red">Under maintenance. Data  may not be available. Please check back later.</span>')->fieldset('data_filters'));
    }
    if ($fields) {
      $form->addButton(
          Set::button()
          ->id('tsv-download')
          ->value('TSV')
          ->fieldset('data_attributes')
          ->ajax(array(
              'callback' => 'tripal_megasearch_ajax_qtl_create_download',
              'wrapper' => 'chado_search-filter-tripal_megasearch-status-field',
              'effect' => 'fade')
              )
          ->attributes(tripal_megasearch_generate_progress_attribute())
          );
      $form->addSubmit(
          Set::submit()
          ->value('CSV')
          ->fieldset('data_attributes')
          ->ajax(array(
            'callback' => 'tripal_megasearch_ajax_qtl_create_download',
            'wrapper' => 'chado_search-filter-tripal_megasearch-status-field',
            'effect' => 'fade')
              )
          ->attributes(tripal_megasearch_generate_progress_attribute())
          );
      $default_rows = \Drupal::state()->get('tripal_megasearch_result_rows', '10');
      if ($default_rows > 0) {
        $form->addButton(
            Set::button()
            ->id('view_results')
            ->fieldset('data_attributes')
            ->value('View')
            ->ajax(array(
              'callback' => 'tripal_megasearch_ajax_qtl_view_result',
              'wrapper' => 'chado_search-tripal_megasearch-markup-results',
              'effect' => 'fade')
                )
            );
      }
      $form->addButton(
          Set::button()
          ->id('apply_filters')
          ->fieldset('data_filters')
          ->value('Refresh Count')
          ->ajax(array(
            'callback' => 'tripal_megasearch_ajax_qtl_refresh_count',
            'wrapper' => 'chado_search-filter-tripal_megasearch-status-field',
            'effect' => 'fade')
              )
          );
      $form->addButton(
          Set::button()
          ->id('clear-all-values')
          ->fieldset('data_filters')
          ->value('Clear')
          ->ajax(array(
              'callback' => 'tripal_megasearch_ajax_filter_clear_values',
              'wrapper' => 'chado_search-filter-tripal_megasearch-data_filters-field',
              'effect' => 'fade')
              )
          );
      $form->addSelectFilter(
          Set::selectFilter()
          ->id('qtl_type')
          ->title('Type')
          ->table('tripal_megasearch_qtl')
          ->column('type')
          ->fieldset('data_filters')
          ->cache(TRUE)
          ->newline()
          );
      $form->addSelectFilter(
          Set::selectFilter()
          ->id('qtl_organism')
          ->title('Organism')
          ->table('tripal_megasearch_qtl')
          ->column('organism')
          ->multiple(TRUE)
          ->searchBox(TRUE)
          ->fieldset('data_filters')
          ->cache(TRUE)
          ->newline()
          );
      // Trait
      $form->addSelectFilter(
          Set::selectFilter()
          ->id('qtl_category')
          ->title('Trait Category')
          ->table('tripal_megasearch_qtl')
          ->column('category')
          ->labelWidth(150)
          ->fieldset('data_filters')
          ->cache(TRUE)
          ->newline()
          );
      if (tripal_megasearch_api_check(4)) {
	      $form->addDynamicSelectFilter(
          Set::dynamicSelectFilter()
          ->id('qtl_trait')
          ->title('Trait Name')
          ->fieldset('data_filters')
          ->labelWidth(150)
          ->cache('tripal_megasearch_qtl', ['category', 'trait'])
          ->cachedOpts('trait', 'category')
          ->dependOnId('qtl_category')
          ->multiple(TRUE)
          ->searchBox(TRUE)
          ->newline()
          );
	      $form->addFieldset(
          Set::fieldset()
          ->id('qtl_group_triat')
          ->title('Trait')
          ->startWidget('qtl_category')
          ->endWidget('qtl_trait')
          );
			}
      // Name
      $form->addTextFilter(
          Set::textFilter()
          ->id('qtl_symbol')
          ->title('Published Symbol')
          ->fieldset('data_filters')
          ->labelWidth(150)
          ->newline()
          );
      $form->addTextFilter(
          Set::textFilter()
          ->id('qtl_qtl')
          ->title('QTL Label')
          ->fieldset('data_filters')
          ->labelWidth(150)
          ->newline()
          );
      $form->addFieldset(
          Set::fieldset()
          ->id('qtl_group_name')
          ->title('Name')
          ->startWidget('qtl_symbol')
          ->endWidget('qtl_qtl')
          );
      //Population
      $form->addTextFilter(
          Set::textFilter()
          ->id('qtl_population')
          ->title('Population')
          ->fieldset('data_filters')
          ->labelWidth(150)
          ->newline()
          );
      $form->addTextFilter(
          Set::textFilter()
          ->id('qtl_maternal_parent')
          ->title('Maternal Parent')
          ->fieldset('data_filters')
          ->labelWidth(150)
          ->newline()
          );
      $form->addTextFilter(
          Set::textFilter()
          ->id('qtl_paternal_parent')
          ->title('Paternal Parent')
          ->fieldset('data_filters')
          ->labelWidth(150)
          ->newline()
          );
      $form->addFieldset(
          Set::fieldset()
          ->id('qtl_group_population')
          ->title('Population')
          ->startWidget('qtl_population')
          ->endWidget('qtl_paternal_parent')
          );
      // Genetic location
      if (tripal_megasearch_api_check(4)) {
        $form->addDynamicSelectFilter(
            Set::dynamicSelectFilter()
            ->id('qtl_map')
            ->title('Map')
            ->dependOnId('qtl_organism')
            ->cache('tripal_megasearch_qtl', ['map', 'organism'])
            ->cachedOpts('map', 'organism')
            ->fieldset('data_filters')
            ->labelWidth(150)
            ->newLine()
            );
        $form->addDynamicSelectFilter(
            Set::dynamicSelectFilter()
            ->id('qtl_lg')
            ->title('Linkage Group')
            ->dependOnId('qtl_map')
            ->cache('tripal_megasearch_qtl', ['lg', 'map'])
            ->cachedOpts('lg', 'map')
            ->fieldset('data_filters')
            ->labelWidth(150)
            ->newLine()
            );
        $form->addTextFilter(
            Set::textFilter()
            ->id('qtl_start')
            ->title('Start')
            ->numeric(TRUE)
            ->fieldset('data_filters')
            ->labelWidth(150)
            ->newline()
            );
        $form->addTextFilter(
            Set::textFilter()
            ->id('qtl_stop')
            ->title('Stop')
            ->numeric(TRUE)
            ->fieldset('data_filters')
            ->labelWidth(150)
            ->newline()
            );
        $form->addFieldset(
            Set::fieldset()
            ->id('qtl_group_genetic_location')
            ->title('Genetic location')
            ->startWidget('qtl_map')
            ->endWidget('qtl_stop')
            );
      }
      // Marker
      $form->addTextFilter(
          Set::textFilter()
          ->id('qtl_col_marker')
          ->title('Colocalizing Marker')
          ->fieldset('data_filters')
          ->labelWidth(150)
          ->newline()
          );
      $form->addTextFilter(
          Set::textFilter()
          ->id('qtl_neighbor_marker')
          ->title('Neighboring Marker')
          ->fieldset('data_filters')
          ->labelWidth(150)
          ->newline()
          );
      $form->addFieldset(
          Set::fieldset()
          ->id('qtl_group_marker')
          ->title('Marker')
          ->startWidget('qtl_col_marker')
          ->endWidget('qtl_neighbor_marker')
          );
      // Statistics
      $form->addTextFilter(
          Set::textFilter()
          ->id('qtl_lod')
          ->title('LOD')
          ->numeric(TRUE)
          ->fieldset('data_filters')
          ->labelWidth(150)
          ->newline()
          );
      $form->addTextFilter(
          Set::textFilter()
          ->id('qtl_r2')
          ->title('R2')
          ->numeric(TRUE)
          ->fieldset('data_filters')
          ->labelWidth(150)
          ->newline()
          );
      $form->addFieldset(
          Set::fieldset()
          ->id('qtl_group_stats')
          ->title('Statistics')
          ->startWidget('qtl_lod')
          ->endWidget('qtl_r2')
          );
      //Publication
      $form->addTextFilter(
          Set::textFilter()
          ->id('qtl_citation')
          ->title('Citation')
          ->fieldset('data_filters')
          ->labelWidth(150)
          ->newline()
          );
      $form->addFieldset(
          Set::fieldset()
          ->id('qtl_group_pub')
          ->title('Publication')
          ->startWidget('qtl_citation')
          ->endWidget('qtl_citation')
          );

      // Select/unselect all fields
      $form->addCheckBoxes(
          Set::checkboxes()
          ->id('attribute_check_all')
          ->options(
              array(
                'select_all' => 'All Fields',
              )
              )
          ->ajax(
              array(
                'callback' => 'tripal_megasearch_ajax_toggle_field_selection',
                'wrapper' => 'chado_search-tripal_megasearch-checkboxes-attribute_checkboxes',
                'effect' => 'fade'
              )
              )
          ->defaultValue(array('select_all'))
          ->fieldset('data_attributes')
          ->newline()
          );
      // Populate attribuet list
      $form->addCheckboxes(
          Set::checkBoxes()
          ->id('attribute_checkboxes')
          ->options($fields)
          ->fieldset('data_attributes')
          ->defaultValue(array_keys($fields))
          );
    }
  }
  return $form;
}

function tripal_megasearch_ajax_qtl_get_filters(&$form_state) {
  $where = array();
  $where [] = Sql::selectFilter('qtl_type', $form_state, 'type');
  $where [] = Sql::selectFilter('qtl_organism', $form_state, 'organism');
  $where [] = Sql::selectFilter('qtl_category', $form_state, 'category');
  $where [] = Sql::selectFilter('qtl_trait', $form_state, 'trait');
  $where [] = Sql::textFilter('qtl_symbol', $form_state, 'symbol');
  $where [] = Sql::textFilter('qtl_qtl', $form_state, 'qtl');
  $where [] = Sql::textFilter('qtl_population', $form_state, 'population');
  $where [] = Sql::textFilter('qtl_maternal_parent', $form_state, 'maternal_parent');
  $where [] = Sql::textFilter('qtl_paternal_parent', $form_state, 'paternal_parent');
  $where [] = Sql::selectFilter('qtl_map', $form_state, 'map');
  $where [] = Sql::selectFilter('qtl_lg', $form_state, 'lg');
  $where [] = Sql::textFilter('qtl_start', $form_state, 'start', FALSE, TRUE);
  $where [] = Sql::textFilter('qtl_stop', $form_state, 'stop', FALSE, TRUE);
  $where [] = Sql::textFilter('qtl_col_marker', $form_state, 'col_marker_uniquename');
  $where [] = Sql::textFilter('qtl_neighbor_marker', $form_state, 'neighbor_marker_uniquename');
  $where [] = Sql::textFilter('qtl_lod', $form_state, 'lod', FALSE, TRUE);
  $where [] = Sql::textFilter('qtl_r2', $form_state, 'r2', FALSE, TRUE);
  $where [] = Sql::textFilter('qtl_citation', $form_state, 'citation');
  return $where;
}

/*
 * Refresh count
 */
function tripal_megasearch_ajax_qtl_refresh_count ($form, &$form_state) {
  $where = tripal_megasearch_ajax_qtl_get_filters($form_state);
  return tripal_megasearch_get_form_status($form, $form_state, $where);
}

function tripal_megasearch_ajax_qtl_view_result ($form, &$form_state) {
  $where = tripal_megasearch_ajax_qtl_get_filters($form_state);
  $result = $form['results'];
  $result['#markup'] = tripal_megasearch_ajax_get_table ($form, $form_state, $where);
  return $result;
}

/*
  * Create download
  */
function tripal_megasearch_ajax_qtl_create_download ($form, &$form_state) {
  $where = tripal_megasearch_ajax_qtl_get_filters($form_state);
  $element = $form_state->getTriggeringElement();
  $format = $element['#value'] == 'TSV' ? 'TSV' : 'CSV';
  return tripal_megasearch_ajax_get_download_file ($form, $form_state, $where, $format);
}
