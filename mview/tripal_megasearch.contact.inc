<?php
function tripal_megasearch_get_contact_mview() {
  $sql = "
  SELECT DISTINCT
    C.contact_id,
    (SELECT name FROM cvterm WHERE cvterm_id = C.type_id) AS type,
    name,
    description,
    LNAME.value AS lname,
    FNAME.value AS fname,
    INST.value AS institution,
    COUNTRY.value AS country,
    RES.value AS research_interest
  FROM contact C
  LEFT JOIN contactprop LNAME ON LNAME.contact_id = C.contact_id AND LNAME.type_id IN (SELECT cvterm_id FROM cvterm WHERE name = 'last_name')
  LEFT JOIN contactprop FNAME ON FNAME.contact_id = C.contact_id AND FNAME.type_id IN (SELECT cvterm_id FROM cvterm WHERE name = 'first_name')
  LEFT JOIN contactprop INST ON INST.contact_id = C.contact_id AND INST.type_id IN (SELECT cvterm_id FROM cvterm WHERE name = 'institution')
  LEFT JOIN contactprop COUNTRY ON COUNTRY .contact_id = C.contact_id AND COUNTRY .type_id IN (SELECT cvterm_id FROM cvterm WHERE name = 'country')
  LEFT JOIN contactprop RES ON RES.contact_id = C.contact_id AND RES.type_id IN (SELECT cvterm_id FROM cvterm WHERE name = 'keywords')
  WHERE C.type_id IS NOT NULL
  ";
  return $sql;
}

function tripal_megasearch_create_contact_mview() {
  $view_name = 'tripal_megasearch_contact';
  chado_search_drop_chado_mview($view_name);
  $schema = array(
    'table' => $view_name,
    'fields' => array(
      'contact_id' => array(
        'type' => 'int',
        'not null' => TRUE,
      ),
      'type' => array (
        'type' => 'varchar',
        'length' => '1024'
      ),
      'name' => array (
        'type' => 'varchar',
        'length' => '255'
      ),
      'description' => array (
        'type' => 'varchar',
        'length' => '255'
      ),
      'lname' => array(
        'type' => 'text'
      ),
      'fname' => array(
        'type' => 'text'
      ),
      'institution' => array(
        'type' => 'text'
      ),
      'country' => array(
        'type' => 'text'
      ),
      'research_interest' => array(
        'type' => 'text'
      ),
    )
  );
  $sql = tripal_megasearch_get_contact_mview();
  chado_search_create_chado_mview($view_name, $schema, $sql);
}