<?php
function tripal_megasearch_get_gene_mview() {
  $sql = "
    SELECT
      F.feature_id,
      F.name,
      F.uniquename,
      F.organism_id,
      (SELECT genus || ' ' || species FROM organism WHERE organism_id = F.organism_id) AS organism,
      (SELECT name FROM cvterm WHERE cvterm_id = F.type_id) AS type,
      ANALYSIS.name AS analysis,
      ANALYSIS.value AS analysis_type,

      (SELECT name FROM analysis A INNER JOIN analysisprop AP ON A.analysis_id = AP.analysis_id AND AP.value = 'whole_genome' WHERE A.analysis_id = (SELECT analysis_id FROM analysisfeature WHERE feature_id = LOC.srcfeature_id LIMIT 1) LIMIT 1) AS genome,
      LOC.srcfeature_id AS landmark_id,
      LOC.landmark,
      LOC.fmin,
      LOC.fmax,
      LOC.landmark|| ':' || fmin || '..' || fmax AS location,

      (SELECT string_agg(distinct
         (SELECT array_to_string(regexp_matches(value, '<Hit_def>(.+?)</Hit_def>'), '')
           FROM analysisfeatureprop AFP2 WHERE AFP2.analysisfeatureprop_id = AFP.analysisfeatureprop_id)
          , '. ')
        FROM analysisfeatureprop AFP
        INNER JOIN analysisfeature AF2 ON AF2.analysisfeature_id = AFP.analysisfeature_id
        AND
          type_id = (SELECT cvterm_id FROM cvterm WHERE name = 'analysis_blast_output_iteration_hits')
        WHERE
        AF2.feature_id = F.feature_id
      ) AS blast,

      (SELECT string_agg(distinct
         (SELECT trim(regexp_replace(value, '<.+>', ''))
           FROM analysisfeatureprop AFP2 WHERE AFP2.analysisfeatureprop_id = AFP.analysisfeatureprop_id)
          , '. ')
        FROM analysisfeatureprop AFP
        INNER JOIN analysisfeature AF2 ON AF2.analysisfeature_id = AFP.analysisfeature_id
        AND
          type_id IN (SELECT cvterm_id FROM cvterm WHERE cv_id = (SELECT cv_id FROM cv WHERE name = 'KEGG_ORTHOLOGY'  or name = 'KEGG_PATHWAYS'))
        WHERE
        AF2.feature_id = F.feature_id
      ) AS kegg,

      (
      SELECT string_agg(distinct value, '. ')
      FROM (
        SELECT
        AF2.feature_id,
        array_to_string (regexp_matches(value, 'name=\"(.+?)\"', 'g'), '') AS value
        FROM analysisfeatureprop AFP2
        INNER JOIN analysisfeature AF2 ON AF2.analysisfeature_id = AFP2.analysisfeature_id
        AND AFP2.type_id = (SELECT cvterm_id FROM cvterm WHERE name = 'analysis_interpro_xmloutput_hit')
        WHERE AF2.feature_id = F.feature_id
      ) INTERPRO GROUP BY feature_id
      ) AS interpro,

      (
      SELECT string_agg(distinct name, '. ')
      FROM (
        SELECT feature_id,
        V.name
        FROM feature_cvterm FC
        INNER JOIN cvterm V ON V.cvterm_id = FC.cvterm_id
        WHERE FC.feature_id = F.feature_id
        AND cv_id IN (SELECT cv_id FROM cv WHERE name IN ('biological_process', 'cellular_component', 'molecular_function'))
      ) GOTERM GROUP BY feature_id
      ) AS go_term,

      (
      SELECT string_agg(distinct acc, '. ')
      FROM (
        SELECT feature_id,
        'GO:' || (SELECT accession FROM dbxref WHERE dbxref_id = V.dbxref_id) AS acc
        FROM feature_cvterm FC
        INNER JOIN cvterm V ON V.cvterm_id = FC.cvterm_id
        WHERE FC.feature_id = F.feature_id
        AND cv_id IN (SELECT cv_id FROM cv WHERE name IN ('biological_process', 'cellular_component', 'molecular_function'))
      ) GOTERM GROUP BY feature_id
      ) AS go_acc,

      (
      SELECT string_agg(value, '. ')
      FROM featureprop
      WHERE type_id IN
        (SELECT cvterm_id
         FROM cvterm
         WHERE name IN ('genbank_note','genbank_gene','product','function','genbank_detail','genbank_description'))
      AND feature_id = F.feature_id
      GROUP BY feature_id
      ) AS gb_keyword
    FROM feature F
    LEFT JOIN (
      SELECT feature_id, A.name, value
      FROM analysis A
      INNER JOIN analysisprop AP ON AP.analysis_id = A.analysis_id AND type_id IN (SELECT cvterm_id FROM cvterm WHERE name = 'Analysis Type')
      AND AP.value IN ('whole_genome', 'reftrans', 'ncbi_data', 'transcriptome')
      INNER JOIN analysisfeature AF ON A.analysis_id = AF.analysis_id
    ) ANALYSIS ON ANALYSIS.feature_id = F.feature_id
    LEFT JOIN (
      SELECT
        max(srcfeature_id) AS srcfeature_id,
        max(FL.feature_id)AS feature_id,
        max((SELECT name FROM feature WHERE feature_id = srcfeature_id)) AS landmark,
        max((fmin + 1)) AS fmin,
        max(fmax) AS fmax
      FROM featureloc FL
      INNER JOIN feature F ON F.feature_id = FL.feature_id
      WHERE (SELECT name FROM cvterm WHERE cvterm_id = (SELECT type_id FROM feature WHERE feature_id = srcfeature_id)) IN ('chromosome', 'supercontig')
      AND F.type_id IN (SELECT cvterm_id FROM cvterm WHERE name IN ('gene', 'mRNA', 'contig'))
      GROUP BY (FL.feature_id, srcfeature_id, fmin, fmax)
    ) LOC ON LOC.feature_id = F.feature_id
    WHERE
    F.type_id IN (SELECT cvterm_id FROM cvterm WHERE name IN ('gene', 'mRNA', 'contig'))";
  return $sql;
}

function tripal_megasearch_create_gene_mview() {
  $view_name = 'tripal_megasearch_gene';
  chado_search_drop_chado_mview($view_name);
  $schema = array(
    'table' => $view_name,
    'fields' => array(
      'feature_id' => array(
        'type' => 'int',
        'not null' => TRUE,
      ),
      'name' => array (
        'type' => 'varchar',
        'length' => '255'
      ),
      'uniquename' => array (
        'type' => 'text',
      ),
      'organism_id' => array(
        'type' => 'int',
      ),
      'organism' => array(
        'type' => 'varchar',
        'length' => '510',
        'not null' => TRUE,
      ),
      'type' => array (
        'type' => 'varchar',
        'length' => '1024'
      ),
      'analysis' => array (
        'type' => 'varchar',
        'length' => '255'
      ),
      'analysis_type' => array(
        'type' => 'text'
      ),
      'genome' => array(
        'type' => 'varchar',
        'length' => '255'
      ),
      'landmark_id' => array(
        'type' => 'int',
      ),
      'landmark' => array (
        'type' => 'varchar',
        'length' => '255'
      ),
      'fmin' => array(
        'type' => 'int'
      ),
      'fmax' => array(
        'type' => 'int'
      ),
      'location' => array(
        'type' => 'text'
      ),
      'blast' => array(
        'type' => 'text'
      ),
      'kegg' => array(
        'type' => 'text'
      ),
      'interpro' => array(
        'type' => 'text'
      ),
      'go' => array(
        'type' => 'text'
      ),
      'go_acc' => array(
        'type' => 'text'
      ),
      'genbank' => array(
        'type' => 'text'
      ),
    )
  );
  $sql = tripal_megasearch_get_gene_mview();
  chado_search_create_chado_mview($view_name, $schema, $sql);
}
