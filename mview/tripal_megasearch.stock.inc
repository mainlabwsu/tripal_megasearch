<?php
function tripal_megasearch_get_stock_mview() {
  $sql = "
  SELECT DISTINCT
    S.stock_id,
    S.name,
    S.uniquename,
    O.genus || ' ' || O.species AS organism,
    COLLECTION.name AS collection,
    DATA.accession,
    MATP.uniquename AS maternal_parent,
    PATP.uniquename AS paternal_parent,
    PEDIGREE.value AS pedigree,
    GEO.country,
    IMG.eimage_data,
    IMG.legend,
    (SELECT string_agg(value, '|') FROM stockprop WHERE stock_id = S.stock_id AND type_id = (SELECT cvterm_id FROM cvterm WHERE name = 'description' AND cv_id = (SELECT cv_id FROM cv WHERE name = 'MAIN'))) AS description,
    (SELECT string_agg(uniquename, '|') FROM pub WHERE pub_id IN (SELECT pub_id FROM stock_pub WHERE stock_id = S.stock_id)) AS pub
  FROM stock S
  INNER JOIN organism O ON S.organism_id = O.organism_id
  LEFT JOIN (
        SELECT stock_id, accession
        FROM dbxref X
        INNER JOIN db ON X.db_id = db.db_id
        INNER JOIN stock_dbxref SX ON X.dbxref_id = SX.dbxref_id
      ) DATA ON DATA.stock_id = S.stock_id
  LEFT JOIN (
        SELECT name, uniquename, stock_id
        FROM stockcollection SC
        INNER JOIN stockcollection_stock SCS ON SC.stockcollection_id = SCS.stockcollection_id
      ) COLLECTION ON COLLECTION.stock_id = S.stock_id
  LEFT JOIN (
    SELECT
      MAT.stock_id,
      MAT.uniquename,
      MSR.object_id
    FROM stock MAT
    INNER JOIN stock_relationship MSR ON MAT.stock_id = MSR.subject_id
    WHERE MSR.type_id = (SELECT cvterm_id FROM cvterm WHERE name = 'is_a_maternal_parent_of')
  ) MATP ON MATP.object_id = S.stock_id
  LEFT JOIN (
    SELECT
      PAT.stock_id,
      PAT.uniquename,
      PSR.object_id
    FROM stock PAT
    INNER JOIN stock_relationship PSR ON PAT.stock_id = PSR.subject_id
    WHERE PSR.type_id = (SELECT cvterm_id FROM cvterm WHERE name = 'is_a_paternal_parent_of')
  ) PATP ON PATP.object_id = S.stock_id
  LEFT JOIN stockprop PEDIGREE ON PEDIGREE.stock_id = S.stock_id AND PEDIGREE.type_id IN (SELECT cvterm_id FROM cvterm WHERE name = 'pedigree')
  LEFT JOIN (
    SELECT
      stock_id,
      COUNTRY.value AS country
    FROM nd_experiment NE
    INNER JOIN nd_experiment_stock NES ON NES.nd_experiment_id = NE.nd_experiment_id
    INNER JOIN nd_geolocation NG ON NG.nd_geolocation_id = NE.nd_geolocation_id
    INNER JOIN nd_geolocationprop COUNTRY ON COUNTRY.nd_geolocation_id = NE.nd_geolocation_id AND COUNTRY.type_id IN (SELECT cvterm_id FROM cvterm WHERE name = 'country')
  ) GEO ON S.stock_id = GEO.stock_id
  lEFT JOIN (
    SELECT stock_id, eimage_data, LEGEND.value AS legend
    FROM stock_image SI
    INNER JOIN eimage I ON SI.eimage_id = I.eimage_id
    LEFT JOIN eimageprop LEGEND ON LEGEND.eimage_id = I.eimage_id AND LEGEND.type_id IN (SELECT cvterm_id FROM cvterm WHERE name = 'legend')
  ) IMG ON IMG.stock_id = S.stock_id
  WHERE S.type_id NOT IN (SELECT cvterm_id FROM cvterm WHERE name IN ('sample', 'clone'))
  ";
  return $sql;
}

function tripal_megasearch_create_stock_mview() {
  $view_name = 'tripal_megasearch_stock';
  chado_search_drop_chado_mview($view_name);
  $schema = array(
    'table' => $view_name,
    'fields' => array(
      'stock_id' => array(
        'type' => 'int',
        'not null' => TRUE,
      ),
      'name' => array (
        'type' => 'varchar',
        'length' => '255'
      ),
      'uniquename' => array (
        'type' => 'text',
      ),
      'organism' => array (
        'type' => 'varchar',
        'length' => 510
      ),
      'collection' => array (
        'type' => 'varchar',
        'length' => '255'
      ),
      'accession' => array (
        'type' => 'varchar',
        'length' => '255'
      ),
      'maternal_parent' => array (
        'type' => 'text',
      ),
      'paternal_parent' => array (
        'type' => 'text',
      ),
      'pedigree' => array(
        'type' => 'text'
      ),
      'country' => array(
        'type' => 'text'
      ),
      'eimage_data' => array(
        'type' => 'text'
      ),
      'legend' => array(
        'type' => 'text'
      ),
      'description' => array (
        'type' => 'text',
      ),
      'pub' => array (
        'type' => 'text',
      ),
    )
  );
  $sql = tripal_megasearch_get_stock_mview();
  chado_search_create_chado_mview($view_name, $schema, $sql);
}