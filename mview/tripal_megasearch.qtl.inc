<?php
function tripal_megasearch_get_qtl_mview() {
  $sql = "
  SELECT DISTINCT
      QTL.feature_id,
      QTL.uniquename AS qtl,
      TRAIT.name AS trait,
      QTL.organism_id,
      (SELECT genus || ' ' || species FROM organism WHERE organism_id = QTL.organism_id) AS organism,
      (CASE WHEN (SELECT name FROM cvterm WHERE cvterm_id = QTL.type_id) = 'heritable_phenotypic_marker' THEN 'MTL' ELSE 'QTL' END) AS type,
      SYMBOL.value AS symbol,
      LOD.value AS lod,
      R2.value AS r2,
      TRAIT.category,
      MAP.name AS map,
      MAP.lg,
      MAP.start,
      MAP.stop,
      CO_LOC_M.uniquename AS colocalizing_marker,
      NEIGHBOR_M.uniquename AS neighboring_marker,
      POP.uniquename AS population,
      MATP.uniquename AS maternal_parent,
      PATP.uniquename AS paternal_parent,
      PUB.uniquename AS citation
    FROM feature QTL
    LEFT JOIN featureprop SYMBOL ON SYMBOL.feature_id = QTL.feature_id AND SYMBOL.type_id IN (SELECT cvterm_id FROM cvterm WHERE name = 'published_symbol')
    LEFT JOIN featureprop LOD ON LOD.feature_id = QTL.feature_id AND LOD.type_id IN (SELECT cvterm_id FROM cvterm WHERE name = 'LOD')
    LEFT JOIN featureprop R2 ON R2.feature_id = QTL.feature_id AND R2.type_id IN (SELECT cvterm_id FROM cvterm WHERE name = 'R2')
    LEFT JOIN (
	   SELECT
	     (SELECT name FROM cvterm WHERE cvterm_id = CATEGORY.cvterm_id) AS category,
         (SELECT name FROM cvterm WHERE cvterm_id = FV.cvterm_id) AS name,
		 feature_id
       FROM feature_cvterm FV
	   LEFT JOIN cvterm_relationship VR ON VR.subject_id = FV.cvterm_id
	   LEFT JOIN cvterm CATEGORY ON CATEGORY.cvterm_id = VR.object_id
	   AND VR.type_id = (SELECT cvterm_id FROM cvterm WHERE name = 'is_a' AND cv_id = (SELECT cv_id FROM cv WHERE name = 'relationship'))
	) TRAIT ON TRAIT.feature_id = QTL.feature_id
    LEFT JOIN
      (SELECT FP.feature_id, FM.featuremap_id, FM.name, LG.name AS lg, START.value AS start, STOP.value AS stop FROM featuremap FM
       INNER JOIN featurepos FP ON FM.featuremap_id = FP.featuremap_id
       INNER JOIN feature LG ON LG.feature_id = FP.map_feature_id
       LEFT JOIN featureposprop START ON START.featurepos_id = FP.featurepos_id AND START.type_id = (SELECT cvterm_id FROM cvterm WHERE name = 'start' AND cv_id = (SELECT cv_id FROM cv WHERE name = 'MAIN'))
       LEFT JOIN featureposprop STOP ON STOP.featurepos_id = FP.featurepos_id AND STOP.type_id = (SELECT cvterm_id FROM cvterm WHERE name = 'stop' AND cv_id = (SELECT cv_id FROM cv WHERE name = 'MAIN'))
       LEFT JOIN featureposprop PEAK ON PEAK.featurepos_id = FP.featurepos_id AND PEAK.type_id = (SELECT cvterm_id FROM cvterm WHERE name = 'qtl_peak' AND cv_id = (SELECT cv_id FROM cv WHERE name = 'MAIN'))
      ) MAP ON MAP.feature_id = QTL.feature_id
    LEFT JOIN
      (SELECT MKR.uniquename, FR.object_id, MKR.feature_id FROM feature MKR
       INNER JOIN feature_relationship FR ON MKR.feature_id = FR.subject_id
       INNER JOIN cvterm V ON V.cvterm_id = FR.type_id
       WHERE V.name = 'located_in'
       AND V.cv_id = (SELECT cv_id FROM cv WHERE name = 'relationship')
      ) CO_LOC_M ON CO_LOC_M.object_id = QTL.feature_id
    LEFT JOIN
      (SELECT MKR.uniquename, FR.object_id, MKR.feature_id FROM feature MKR
       INNER JOIN feature_relationship FR ON MKR.feature_id = FR.subject_id
       INNER JOIN cvterm V ON V.cvterm_id = FR.type_id
       WHERE V.name = 'adjacent_to'
       AND V.cv_id = (SELECT cv_id FROM cv WHERE name = 'relationship')
      ) NEIGHBOR_M ON NEIGHBOR_M.object_id = QTL.feature_id
    LEFT JOIN
      (SELECT S.stock_id, featuremap_id, S.uniquename FROM stock S
       INNER JOIN featuremap_stock FS ON S.stock_id = FS.stock_id
      ) POP ON POP.featuremap_id = MAP.featuremap_id
    LEFT JOIN (
      SELECT
        MAT.stock_id,
        MAT.uniquename,
        MSR.object_id
      FROM stock MAT
      INNER JOIN stock_relationship MSR ON MAT.stock_id = MSR.subject_id
      WHERE MSR.type_id = (SELECT cvterm_id FROM cvterm WHERE name = 'is_a_maternal_parent_of')
    ) MATP ON MATP.object_id = POP.stock_id
    LEFT JOIN (
      SELECT
        PAT.stock_id,
        PAT.uniquename,
        PSR.object_id
      FROM stock PAT
      INNER JOIN stock_relationship PSR ON PAT.stock_id = PSR.subject_id
      WHERE PSR.type_id = (SELECT cvterm_id FROM cvterm WHERE name = 'is_a_paternal_parent_of')
    ) PATP ON PATP.object_id = POP.stock_id
    LEFT JOIN
      (SELECT P.pub_id, uniquename, feature_id FROM pub P
       INNER JOIN feature_pub FP ON FP.pub_id = P.pub_id
       ) PUB ON PUB.feature_id = QTL.feature_id
    WHERE QTL.type_id IN (SELECT cvterm_id FROM cvterm WHERE name IN ('QTL', 'heritable_phenotypic_marker') AND cv_id = (SELECT cv_id FROM cv WHERE name = 'sequence'))
  ";
  return $sql;
}

function tripal_megasearch_create_qtl_mview() {
  $view_name = 'tripal_megasearch_qtl';
  chado_search_drop_chado_mview($view_name);
  $schema = array (
    'table' => $view_name,
    'fields' => array(
      'feature_id' => array(
        'type' => 'int',
      ),
      'qtl' => array(
        'type' => 'text',
        'not null' => TRUE,
      ),
      'trait' => array(
        'type' => 'varchar',
        'length' => '255',
      ),
      'organism_id' => array(
        'type' => 'int',
      ),
      'organism' => array(
        'type' => 'varchar',
        'length' => '510',
        'not null' => TRUE,
      ),
      'type' => array(
        'type' => 'varchar',
        'length' => '1024',
        'not null' => TRUE,
      ),
      'symbol' => array(
        'type' => 'text',
      ),
      'lod' => array(
        'type' => 'text',
      ),
      'r2' => array(
        'type' => 'text',
      ),
      'category' => array(
        'type' => 'varchar',
        'length' => '1024',
      ),
      'map' => array(
        'type' => 'text',
      ),
      'lg' => array(
        'type' => 'varchar',
        'length' => '255',
      ),
      'start' => array(
        'type' => 'text',
      ),
      'stop' => array(
        'type' => 'text',
      ),
      'qtl_peak' => array(
        'type' => 'text',
      ),
      'col_marker_uniquename' => array(
        'type' => 'text',
      ),
      'neighbor_marker_uniquename' => array(
        'type' => 'text',
      ),
      'population' => array(
        'type' => 'text',
      ),
      'maternal_parent' => array (
        'type' => 'text',
      ),
      'paternal_parent' => array (
        'type' => 'text',
      ),
      'citation' => array(
        'type' => 'text',
      ),
    ),
  );
  $sql = tripal_megasearch_get_qtl_mview();
  chado_search_create_chado_mview($view_name, $schema, $sql);
}