<?php
function tripal_megasearch_get_marker_mview() {
  $sql = "
    SELECT DISTINCT
      MARKER.feature_id,
      MARKER.uniquename,
      MARKER.name,
      MARKER.residues,
      O.organism_id,
      O.genus || ' ' || O.species AS organism,
      MAP.map,
      STK.mapped_organism,
      MAP.lg,
      UPPER(replace(MTYPE.value, '_', ' ')) AS marker_type,
      cast(START.value as real) AS start,
      cast(STOP.value as real) AS stop,

        (
         SELECT  
           (SELECT name FROM analysis N WHERE N.analysis_id = AP.analysis_id) 
         FROM analysisfeature AF 
		 INNER JOIN analysisprop AP ON AP.analysis_id = AF.analysis_id
		 WHERE AF.feature_id = LOC.srcfeature_id
         AND AP.type_id = (SELECT cvterm_id FROM cvterm WHERE name = 'Analysis Type' AND cv_id = (SELECT cv_id FROM cv WHERE name = 'analysis_property'))
		 AND AP.value = 'whole_genome' LIMIT 1
        ) AS genome,
      
      LOC.srcfeature_id AS landmark_id,
      LOC.name AS landmark,
      (LOC.fmin + 1) AS fmin,
      LOC.fmax,
      LOC.name || ':' || (fmin + 1) || '..' || fmax AS location,
      ALIAS.value AS alias,
      SYNONYM.value AS synonym,
      TRAIT.category,
      TRAIT.trait,
      ARR.snp_array,
      ARR.array_id,
      DBSNP.accession AS dbsnp_id,
      PRIMER.primers,
      PROBE.probes,
	    (SELECT string_agg((SELECT uniquename FROM pub P WHERE P.pub_id = FP.pub_id), '. ') FROM feature_pub FP WHERE FP.feature_id = MARKER.feature_id GROUP BY FP.feature_id) AS pubs,
      ALLELE.value AS allele,
      (SELECT value FROM featureprop WHERE feature_id = MARKER.feature_id AND type_id = (SELECT cvterm_id FROM cvterm WHERE name = 'five_prime_flanking_region' AND cv_id = (SELECT cv_id FROM cv WHERE name = 'sequence')) LIMIT 1) AS five_prime_flaking,
      (SELECT value FROM featureprop WHERE feature_id = MARKER.feature_id AND type_id = (SELECT cvterm_id FROM cvterm WHERE name = 'three_prime_flanking_region' AND cv_id = (SELECT cv_id FROM cv WHERE name = 'sequence')) LIMIT 1) AS three_prime_flanking

      FROM feature MARKER

      INNER JOIN organism O ON O.organism_id = MARKER.organism_id

      LEFT JOIN
        (SELECT
           object_id,
           featuremap_id,
           (SELECT name FROM featuremap WHERE featuremap_id = FS.featuremap_id) AS map,
           (SELECT name FROM feature WHERE feature_id = map_feature_id) AS lg,
           map_feature_id,
           featurepos_id
         FROM feature LOCUS
         INNER JOIN feature_relationship FR ON FR.subject_id = LOCUS.feature_id
         INNER JOIN featurepos FS ON FS.feature_id = LOCUS.feature_id
         WHERE LOCUS.type_id = (SELECT cvterm_id FROM cvterm WHERE name = 'marker_locus' AND cv_id = (SELECT cv_id FROM cv WHERE name = 'MAIN'))
         AND FR.type_id = (SELECT cvterm_id FROM cvterm WHERE name = 'instance_of' AND cv_id = (SELECT cv_id FROM cv WHERE name = 'relationship'))
        ) MAP ON MAP.object_id = MARKER.feature_id

      LEFT JOIN
        (SELECT
           featuremap_id, genus || ' ' || species AS mapped_organism,
           O.organism_id
         FROM featuremap_stock FMS
         INNER JOIN stock S ON S.stock_id = FMS.stock_id
         INNER JOIN organism O ON S.organism_id = O.organism_id
      ) STK ON STK.featuremap_id = MAP.featuremap_id

      LEFT JOIN
        (SELECT DISTINCT feature_id, value
         FROM featureprop FP
         WHERE
           FP.type_id = (SELECT cvterm_id FROM cvterm WHERE name = 'marker_type' AND cv_id = (SELECT cv_id FROM cv WHERE name = 'MAIN'))
        ) MTYPE ON MTYPE.feature_id = MARKER.feature_id

      LEFT JOIN
        (SELECT featurepos_id, value
         FROM featureposprop
         WHERE
           type_id = (SELECT cvterm_id FROM cvterm WHERE name = 'start' AND cv_id = (SELECT cv_id FROM cv WHERE name = 'MAIN'))
        ) START ON START.featurepos_id = MAP.featurepos_id

      LEFT JOIN
        (SELECT featurepos_id, value
         FROM featureposprop
         WHERE
           type_id = (SELECT cvterm_id FROM cvterm WHERE name = 'stop' AND cv_id = (SELECT cv_id FROM cv WHERE name = 'MAIN'))
      ) STOP ON STOP.featurepos_id = MAP.featurepos_id

      LEFT JOIN
        (SELECT
           max(FL.feature_id) AS feature_id,
           max(srcfeature_id) AS srcfeature_id,
           max(F.name) AS name,
           max(F.uniquename) AS uniquename,
           max(fmin) AS fmin,
           max(fmax) AS fmax
        FROM featureloc FL
        INNER JOIN feature F ON F.feature_id = FL.srcfeature_id
        INNER JOIN feature F2 ON F2.feature_id = FL.feature_id
        WHERE
          (F.type_id IN (SELECT cvterm_id FROM cvterm WHERE name = 'chromosome' AND cv_id = (SELECT cv_id FROM cv WHERE name = 'sequence'))
          OR
           F.type_id IN (SELECT cvterm_id FROM cvterm WHERE name = 'supercontig' AND cv_id = (SELECT cv_id FROM cv WHERE name = 'sequence')))
        AND F2.type_id = (SELECT cvterm_id FROM cvterm WHERE name = 'genetic_marker' AND cv_id = (SELECT cv_id FROM cv WHERE name = 'sequence'))
        GROUP BY (FL.feature_id, srcfeature_id, F.name, F.uniquename, fmin, fmax)
      ) LOC ON LOC.feature_id = MARKER.feature_id

      LEFT JOIN
        (SELECT
           feature_id,
           string_agg(value, '. ') AS value
         FROM featureprop FP
         WHERE
           FP.type_id = (SELECT cvterm_id FROM cvterm WHERE name = 'alias' AND cv_id = (SELECT cv_id FROM cv WHERE name = 'MAIN'))
         GROUP BY feature_id
      ) ALIAS ON ALIAS.feature_id = MARKER.feature_id

      LEFT JOIN
        (SELECT
            feature_id,
            string_agg(distinct name, '.') AS value
          FROM synonym S
          INNER JOIN feature_synonym FS ON S.synonym_id = FS.synonym_id
          GROUP BY feature_id
      ) SYNONYM  ON SYNONYM.feature_id = MARKER.feature_id

      LEFT JOIN (
  	   SELECT 
  	     (SELECT name FROM cvterm WHERE cvterm_id = CATEGORY.cvterm_id) AS category,
           (SELECT name FROM cvterm WHERE cvterm_id = FV.cvterm_id) AS trait,
    		 FR.subject_id
           FROM feature_cvterm FV
    	   INNER JOIN feature_relationship FR ON FR.object_id = FV.feature_id
    	   AND FR.type_id IN (SELECT cvterm_id FROM cvterm WHERE name IN ('adjacent_to', 'located_in') AND cv_id = (SELECT cv_id FROM cv WHERE name = 'relationship'))
    	   LEFT JOIN cvterm_relationship VR ON VR.subject_id = FV.cvterm_id
    	   LEFT JOIN cvterm CATEGORY ON CATEGORY.cvterm_id = VR.object_id
    	   AND VR.type_id = (SELECT cvterm_id FROM cvterm WHERE name = 'is_a' AND cv_id = (SELECT cv_id FROM cv WHERE name = 'relationship'))
  	 	) TRAIT ON TRAIT.subject_id = MARKER.feature_id

      LEFT JOIN (
        SELECT
          FS.feature_id, S.name AS array_id, L.name AS snp_array
        FROM feature_synonym FS
        INNER JOIN synonym S ON FS.synonym_id = S.synonym_id
        INNER JOIN library_feature LF ON LF.feature_id = FS.feature_id
        INNER JOIN library L ON L.library_id = LF.library_id
        WHERE S.type_id = (SELECT cvterm_id FROM cvterm WHERE name = 'SNP_chip' AND cv_id = (SELECT cv_id FROM cv WHERE name = 'MAIN'))
      ) ARR ON ARR.feature_id = MARKER.feature_id

      LEFT JOIN (
        SELECT accession, feature_id
        FROM dbxref X
        INNER JOIN feature_dbxref FD ON X.dbxref_id = FD.dbxref_id
        WHERE db_id = (SELECT db_id FROM db WHERE name = 'dbSNP')
      ) DBSNP ON DBSNP.feature_id = MARKER.feature_id

      LEFT JOIN (
        SELECT F.feature_id, string_agg(P.name || ':' || P.residues, '|') AS primers
        FROM feature F
        INNER JOIN feature_relationship FR ON F.feature_id = FR.subject_id
        INNER JOIN feature P ON P.feature_id = FR.object_id
        WHERE
          F.type_id = (SELECT cvterm_id FROM cvterm WHERE name = 'genetic_marker' AND cv_id = (SELECT cv_id FROM cv WHERE name = 'sequence'))
        AND
          P.type_id = (SELECT cvterm_id FROM cvterm WHERE name = 'primer' AND cv_id = (SELECT cv_id FROM cv WHERE name = 'sequence'))
        AND
          FR.type_id = (SELECT cvterm_id FROM cvterm WHERE name = 'adjacent_to' AND cv_id = (SELECT cv_id FROM cv WHERE name = 'relationship'))
        GROUP BY F.feature_id
      ) PRIMER ON PRIMER.feature_id = MARKER.feature_id

      LEFT JOIN (
        SELECT F.feature_id, string_agg(P.name || ':' || P.residues, '|') AS probes
        FROM feature F
        INNER JOIN feature_relationship FR ON F.feature_id = FR.object_id
        INNER JOIN feature P ON P.feature_id = FR.subject_id
        WHERE
          F.type_id = (SELECT cvterm_id FROM cvterm WHERE name = 'genetic_marker' AND cv_id = (SELECT cv_id FROM cv WHERE name = 'sequence'))
        AND
          P.type_id = (SELECT cvterm_id FROM cvterm WHERE name = 'probe' AND cv_id = (SELECT cv_id FROM cv WHERE name = 'sequence'))
        AND
          FR.type_id = (SELECT cvterm_id FROM cvterm WHERE name = 'associated_with' AND cv_id = (SELECT cv_id FROM cv WHERE name = 'sequence'))
        GROUP BY F.feature_id
      ) PROBE ON PROBE.feature_id = MARKER.feature_id

      LEFT JOIN
        (SELECT feature_id, max(value) AS value FROM featureprop WHERE type_id IN
           (SELECT cvterm_id FROM cvterm
            WHERE name IN ('allele', 'SNP')
           )
         GROUP BY feature_id
        ) ALLELE ON ALLELE.feature_id = MARKER.feature_id

      WHERE MARKER.type_id = (SELECT cvterm_id FROM cvterm WHERE name = 'genetic_marker' AND cv_id = (SELECT cv_id FROM cv WHERE name = 'sequence'))
  ";
  return $sql;
}

function tripal_megasearch_create_marker_mview() {
  $view_name = 'tripal_megasearch_marker';
  chado_search_drop_chado_mview($view_name);
  $schema = array(
    'table' => $view_name,
    'fields' => array(
      'feature_id' => array(
        'type' => 'int',
        'not null' => TRUE,
      ),
      'uniquename' => array(
        'type' => 'text'
      ),
      'name' => array (
        'type' => 'varchar',
        'length' => '255'
      ),
      'residues' => array(
        'type' => 'text'
      ),
      'organism_id' => array(
        'type' => 'int'
      ),
      'organism' => array (
        'type' => 'varchar',
        'length' => '510'
      ),
      'map' => array (
        'type' => 'varchar',
        'length' => '255'
      ),
      'mapped_organism' => array (
        'type' => 'varchar',
        'length' => '510'
      ),
      'lg' => array (
        'type' => 'varchar',
        'length' => '255'
      ),
      'marker_type' => array(
        'type' => 'text'
      ),
      'start' => array(
        'type' => 'float'
      ),
      'stop' => array(
        'type' => 'float'
      ),
      'genome' => array (
        'type' => 'varchar',
        'length' => '255'
      ),
      'landmark_id' => array(
        'type' => 'int',
      ),
      'landmark' => array (
        'type' => 'varchar',
        'length' => '255'
      ),
      'fmin' => array (
        'type' => 'int'
      ),
      'fmax' => array (
        'type' => 'int'
      ),
      'location' => array (
        'type' => 'varchar',
        'length' => '510'
      ),
      'alias' => array (
        'type' => 'text'
      ),
      'synonym' => array (
        'type' => 'text'
      ),
      'category' => array (
        'type' => 'varchar',
        'length' => '255'
      ),
      'trait' => array (
        'type' => 'varchar',
        'length' => '255'
      ),
      'snp_array' => array(
        'type' => 'varchar',
        'length' => '255'
      ),
      'array_id' => array(
        'type' => 'varchar',
        'length' => '255'
      ),
      'dbsnp_id' => array (
        'type' => 'varchar',
        'length' => '255'
      ),
      'primers' => array (
        'type' => 'text'
      ),
      'probes' => array (
        'type' => 'text'
      ),
      'pubs' => array (
        'type' => 'text'
      ),
      'allele' => array (
        'type' => 'text'
      ),
      'five_prime_flanking' => array (
        'type' => 'text'
      ),
      'three_prime_flanking' => array (
        'type' => 'text'
      ),
    )
  );
  $sql = tripal_megasearch_get_marker_mview();
  chado_search_create_chado_mview($view_name, $schema, $sql);
}