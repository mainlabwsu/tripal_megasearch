// Fix Boostrap theme that highlights all megasearch related menu items
jQuery(document).ready(function () {
      //jQuery('.dropdown-menu li').removeClass('active');
      var path = window.location.pathname;
      if (path == '/tripal_megasearch') {
        if (window.location.search != '') {
          path += window.location.search;
        };
        jQuery('.dropdown-menu li').each(function (index) {
          if (jQuery(this).find('a').attr('href') != path) {
            jQuery(this).removeClass('active');
          }
          else {
            jQuery(this).addClass('active');
          }
        })
      }
})