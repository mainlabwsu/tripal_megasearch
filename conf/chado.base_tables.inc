<?php 

/**
 * tripal_megasearch_data_definition()
 * 
 * Return the definition of the data (mviews or tables)
 * 
 * $def[$table] = array (                                                 // $table should match the table name in the database
 *   'name' => 'dataset_name',                                     // The name to show in the Data Type dropdown
 *   'field' => array (                                                        // Fields to show in both Dynamic Form Filter dropdown and Downloadable Fields checkboxes
 *     $column1 => 'label1',                                            // $column should match the column in the table
 *     $column2 => 'label2',
 *     ...
 *   )
 *   'field_link_callback' => array (                                // (Optional) Hyperlink callback function that returns a URL for fields
 *     $column1 => '$callback:$column1,$column2,...'  // $column should match the column in the table. $callback is a valid PHP callback that returns a URL for link-out. $column1, $column2, etc will be passed in to the callback as arguments
 *     $column2 => '$callback:$column1,$column2,...',
 *     ...
 *   )
 *   'form' => 'static_form_callback_function',           // (Optional) Callback for the Static Form. If this is not provided or callback does not exist, fall back to use the Dynamic Form
 *   'file' => 'custom_dir/custom_file.inc'                   // (Optional) Include specified file when searching for the callback. Accept only relative path from the module root directory. Create custom sub-directory accordingly.
 *   'fasta' => 'column_to_provide_fasta_download' // (Optional) Provide a FASTA download using specified column (usually feature_id)
 *   'count' => 'column_to_count_unique_records'   // (Optional) Column for counting unique records (e.g. stock_id)
 *   'duplicates' => 'not_remove'                      // (Optional) Overriding the administrative 'remove duplicate' setting for this dataset. Allowed values are 'remove' and 'not_remove'
 * )
 * 
 * @return $def array
 */
function tripal_megasearch_data_definition () {
  $def = array();
  $def = tripal_megasearch_analysis ($def);
  $def = tripal_megasearch_contact ($def);
  $def = tripal_megasearch_feature ($def);
  $def = tripal_megasearch_featuremap ($def);
  $def = tripal_megasearch_library ($def);
  $def = tripal_megasearch_organism ($def);
  $def = tripal_megasearch_pub ($def);
  $def = tripal_megasearch_stock ($def);
  return $def;
}

function tripal_megasearch_analysis ($def = array()) {
  $def['analysis'] = array(
    'name' => 'Analysis',
    'field' => array(
      'analysis_id' => 'ID',
      'name' => 'Name',
      'description' => 'Description',
      'program' => 'Program',
      'programversion' => 'Program Version',
      'algorithm' => 'Algorithm',
      'sourcename' => 'Source Name',
      'sourceversion' => 'Source Version',
      'sourceuri' => 'Source URI',
      'timeexecuted' => 'Time Executed'
    ),
  );
  return $def;
}

function tripal_megasearch_contact ($def = array()) {
  $def['contact'] = array(
    'name' => 'Contact',
    'field' => array(
      'contact_id' => 'ID',
      'type_id' => 'Type ID',
      'name' => 'Name',
      'description' => 'Description',
    ),
  );
  return $def;
}

function tripal_megasearch_feature ($def = array()) {
  $def['feature'] = array(
    'name' => 'Feature',
    'field' => array(
      'feature_id' => 'ID',
      'name' => 'Name',
      'uniquename' => 'Unique Name',
      'residues' => 'Residues',
      'seqlen' => 'Sequence Length',
      'md5checksum' => 'MD5',
      'type_id' => 'Type ID',
    ),
    'fasta' => 'feature_id',
  );
  return $def;
}

function tripal_megasearch_featuremap ($def = array()) {
  $def['featuremap'] = array(
    'name' => 'Map',
    'field' => array(
      'featuremap_id' => 'ID',
      'name' => 'Map Name',
      'description' => 'Description',
      'unittype_id' => 'Unit Type ID',
    ),
  );
  return $def;
}

function tripal_megasearch_library ($def = array()) {
  $def['library'] = array(
    'name' => 'Library',
    'field' => array(
      'library_id' => 'ID',
      'name' => 'Name',
      'uniquename' => 'Uniquename',
      'type_id' => 'Type ID',
    ),
  );
  return $def;
}

function tripal_megasearch_organism ($def = array()) {
  $def['organism'] = array(
    'name' => 'Organism',
    'field' => array(
      'organism_id' => 'ID',
      'abbreviation' => 'Abbreviation',
      'genus' => 'Genus',
      'species' => 'Species',
      'common_name' => 'Common Name',
      'comment' => 'Comment'
    ),
  );
  return $def;
}

function tripal_megasearch_project ($def = array()) {
  $def['project'] = array(
    'name' => 'Project',
    'field' => array(
      'project_id' => 'ID',
      'name' => 'Name',
      'description' => 'Description',
    ),
  );
  return $def;
}

function tripal_megasearch_pub ($def = array()) {
  $def['pub'] = array(
    'name' => 'Publication',
    'field' => array(
      'pub_id' => 'ID',
      'title' => 'Title',
      'volumetitle' => 'Volume Title',
      'volume' => 'Volume',
      'series_name' => 'Journal Name',
      'issue' => 'Issue',
      'pyear' => 'Year',
      'pages' => 'Pages',
      'miniref' => 'Mini Reference',
      'uniquename' => 'Uniquename',
      'type_id' => 'Type ID',
      'publisher' => 'Publisher',
      'pubplace' => 'Pub Place',
    ),
  );
  return $def;
}

function tripal_megasearch_stock ($def = array()) {
  $def['stock'] = array(
    'name' => 'Stock',
    'field' => array(
      'stock_id' => 'ID',
      'name' => 'Name',
      'uniquename' => 'Unique Name',
      'description' => 'Description',
      'type_id' => 'Type ID',
    ),
  );
  return $def;
}