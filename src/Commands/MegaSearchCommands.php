<?php

namespace Drupal\tripal_megasearch\Commands;

use Drush\Commands\DrushCommands;

/**
 * Drush commands
 */
class MegaSearchCommands extends DrushCommands {

  /**
   * Recreate a Tripal MegaSearch MView.
   *
   * @command tripal-megasearch:mview-recreate
   * @aliases trpms-mvr
   * @options name
   *   The MView name to recreate.
   * @usage mview-recreate --name=[MVIEW]
   *   Drop and recreate a Tripal MegaSearch MView
   */
  public function mviewRecreate($options = ['name' => NULL]) {
    $mview = $options['name'];
    if (!$mview) {
      print "Please specify the MView name by using --name option\n";
      return;
    }
    $settings = chado_search_get_mview('tripal_megasearch_data_definition', $mview);
    if ($settings) {
      print "Recreating MView '$mview'...\n";
      $func = str_replace('tripal_megasearch_', 'tripal_megasearch_create_', $mview) . '_mview';
      if (isset($settings['mview_file'])) {
        \Drupal::moduleHandler()->loadInclude('tripal_megasearch', 'inc', str_replace('.inc', '', $settings['mview_file']));
        print "Requiring custom mview file '" . $settings['mview_file'] . "'...\n";
      }
      else {
        \Drupal::moduleHandler()->loadInclude('tripal_megasearch', 'inc', 'mview/' . str_replace('tripal_megasearch_', 'tripal_megasearch.', $mview));
      }
      if (function_exists($func)) {
        $func();
      }
      else {
        print "Error: '$mview' is not a Tripal MegaSearch supporting MView.\n";
      }
    }
    else {
      print "Error: MView '$mview' not found. Please check your spelling.\n";
    }
  }

  /**
   * List all Tripal MegaSearch MViews.
   *
   * @command tripal-megasearch:mview-list
   * @aliases trpms-mvl
   * @usage mview-list
   *   List all Tripal MegaSearch MViews
   */
  public function mviewList($options = []) {
    $def = tripal_megasearch_data_definition();
    $mviews = array_keys($def);
    print 'The following ' . count($mviews) . " Tripal MegaSearch MViews are enabled: \n";
    foreach($mviews AS $idx => $mv) {
        $time = \Drupal::state()->get('chado_search_time_' . $mv);
        $rows = \Drupal::state()->get('chado_search_rows_' . $mv);
        print '   ' . ($idx + 1) . '. ' . $mv;
        if ($rows) {
          print " \t ($rows rows)";
        }
        if ($time) {
          $time = date( "Y-m-d h:i:sa", $time);
          print " \t populated on $time";
        }
        print "\n";
    }
  }
  

  /**
   * Populate a Tripal MegaSearch MView.
   *
   * @command tripal-megasearch:mview-populate
   * @aliases trpms-mvp
   * @options name
   *   The MView name to populate.
   * @usage mview-populate --name=[MVIEW]
   *   Populate a Tripal MegaSearch MView
   */
  public function mviewPopulate($options = ['name' => NULL]) {
    $mview = $options['name'];
    if (!$mview) {
      print "Please specify the MView name by using --name option\n";
      return;
    }
    $settings = chado_search_get_mview('tripal_megasearch_data_definition', $mview);
    if ($settings) {
      $func = str_replace('tripal_megasearch_', 'tripal_megasearch_get_', $mview) . '_mview';
      if (isset($settings['mview_file'])) {
        \Drupal::moduleHandler()->loadInclude('tripal_megasearch', 'inc', str_replace('.inc', '', $settings['mview_file']));
        print "Requiring custom mview file '" . $settings['mview_file'] . "'...\n";
      }
      else {
        \Drupal::moduleHandler()->loadInclude('tripal_megasearch', 'inc', 'mview/' . str_replace('tripal_megasearch_', 'tripal_megasearch.', $mview));
      }
      if (function_exists($func)) {
        $sql = $func();
        chado_search_populate_mview_transaction($mview, $sql);
      }
      else {
        print "Error: '$mview' is not a Tripal MegaSearch supporting MView.\n";
      }
    }
    else {
      print "Error: MView '$mview' not found. Please check your spelling.\n";
    }
  }
  
  /**
   * Incrementally Populate a Tripal MegaSearch MView.
   *
   * @command tripal-megasearch:mview-incremental
   * @aliases trpms-mvi
   * @options name
   *   The MView name to populate.
   * @options batch_size
   *   Limit the MView population to this batch size
   * @usage mview-incremental --name=[MVIEW]
   *   Incrementally Populate a Tripal MegaSearch MView
   */
  public function mviewIncremental($options = ['name' => NULL, 'batch_size' => NULL]) {
    $mview = $options['name'];
    $batch_size = $options['batch_size'];
    if (!$mview) {
      print "Please specify the MView name by using --name option\n";
      return;
    }
    $settings = chado_search_get_mview('tripal_megasearch_data_definition', $mview);
    if ($settings) {
      $func = str_replace('tripal_megasearch_', 'tripal_megasearch_get_', $mview) . '_mview';
      if (isset($settings['mview_file'])) {
        \Drupal::moduleHandler()->loadInclude('tripal_megasearch', 'inc', str_replace('.inc', '', $settings['mview_file']));
        print "Requiring custom mview file '" . $settings['mview_file'] . "'...\n";
      }
      else {
        \Drupal::moduleHandler()->loadInclude('tripal_megasearch', 'inc', 'mview/' . str_replace('tripal_megasearch_', 'tripal_megasearch.', $mview));
      }
      if (function_exists($func)) {
        $sql = $func();
        $col_count = isset($settings['count']) ? $settings['count'] : NULL;
        if ($col_count) {
          chado_search_populate_mview_incremental($mview, $sql, $col_count, $batch_size);
        }
        else {
          print "Error: '$mview' does not support incremental population.\n";
        }
      }
      else {
        print "Error: '$mview' is not a Tripal MegaSearch supporting MView.\n";
      }
    }
    else {
      print "Error: MView '$mview' not found. Please check your spelling.\n";
    }
  }
  
  /**
   * Purge a Tripal MegaSearch MView.
   *
   * @command tripal-megasearch:mview-purge
   * @aliases trpms-mvg
   * @options name
   *   The MView name to populate.
   * @usage mview-purge --name=[MVIEW]
   *   Purge a Tripal MegaSearch MView
   */
  public function mviewPurge($options = ['name' => NULL]) {
    $mview = $options['name'];
    if (!$mview) {
      print "Please specify the MView name by using --name option\n";
      return;
    }
    $settings = chado_search_get_mview('tripal_megasearch_data_definition', $mview);
    if ($settings) {
      print "Purging MView '$mview' ...\n";
      $settings = chado_search_get_mview('tripal_megasearch_data_definition', $mview);
      $col_count = isset($settings['count']) ? $settings['count'] : NULL;
      $base_table = str_replace('_id', '', $col_count);
      
      if ($col_count && \Drupal::database()->schema()->tableExists('chado.' . $base_table)) {
        $sql = "DELETE FROM tripal_megasearch_gene WHERE feature_id NOT IN (SELECT feature_id FROM feature)";
        chado_search_query($sql, [], 'chado');
        print "Done.\n";
      }
    }
    else {
      print "Error: MView '$mview' not found. Please check your spelling.\n";
    }
  }
}