<?php
namespace Drupal\tripal_megasearch\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

class AdminForm extends FormBase {

    /**
     * {@inheritdoc}
     */
     public function getFormId() {
        return 'tripal_megasearch_admin_form';
    }

    public function buildForm(array $form, FormStateInterface $form_state, $search_id = NULL) {

    $default_mview = \Drupal::state()->get('tripal_megasearch_data_file', 'tripal_megasearch.mviews.inc');
        $dir = \Drupal::service('extension.list.module')->getPath('tripal_megasearch') . '/conf';
        $files = scandir($dir);
        $options = array();
        foreach ($files AS $f) {
            if ($f != '.' && $f != '..') {
            if (preg_match('/\.inc$/',$f)) {
                $options [$f] = $f;
            }
            }
        }
        $form['data_file'] = array(
            '#type' => 'radios',
            '#title' => 'Data Source',
            '#description' => 'The file that defines all downloadable data as tables or materialized views (MView) in a function named \'tripal_megasearch_data_definition\' which returns a PHP associative array. This file should be in the \'conf\' sub-directory and have an extension of .inc. The definition will be used for creating dynamic form filters and downloadable fields. You can create your own data source file that meets above requirements which will then show up here as an option.',
            '#options' => $options,
            '#default_value' => $default_mview
        );
        $default_query_form = \Drupal::state()->get('tripal_megasearch_query_form', 'dynamic');
        $form['query_form'] = array(
            '#type' => 'radios',
            '#title' => 'Query Form',
            '#description' => 'There are two types of query forms. The dynamic form allows users to add filters dynamically according to the columns of a table or MView defined in the data source. The static forms are pre-defined forms that work best with the MViews preinstalled by Tripal MegaSearch. However, they may not suit everyone if data are stored in different ways. Note: If static forms are not defined, Tripal MegaSearch will fall back to use the dynamic form.',
            '#options' => array('dynamic' => 'Dynamic Form', 'static' => 'Static Form'),
            '#default_value' => $default_query_form
        );
        $default_autocomplete = \Drupal::state()->get('tripal_megasearch_dynamic_form_autocomplete', 'off');
        $form['dynamic_form_autocomplete'] = array(
            '#type' => 'radios',
            '#title' => 'Dynamic Form Autocomplete',
            '#description' => 'Turn autocomplete on or off for the dynamic form. If this is on, the filters will show matching values once the user starts typing.',
            '#options' => array('on' => 'On', 'off' => 'Off'),
            '#default_value' => $default_autocomplete
        );
        $default_link = \Drupal::state()->get('tripal_megasearch_link_in_new_tab', 'off');
        $form['link_in_new_tab'] = array(
            '#type' => 'radios',
            '#title' => 'Open links in new tab',
            '#description' => 'Open links in a new browser tab instead of the current tab.',
            '#options' => array('on' => 'On', 'off' => 'Off'),
            '#default_value' => $default_link
        );
        $default_limit = \Drupal::state()->get('tripal_megasearch_download_limit', '200000');
        $form['download_limit'] = array(
            '#type' => 'textfield',
            '#title' => 'Download Limit',
            '#description' => 'The download limit on each query. Increase this number may add to the server load. Set to 0 for unlimited download. (Default = 200000)',
            '#default_value' => $default_limit
        );
        $default_fasta_limit = \Drupal::state()->get('tripal_megasearch_fasta_download_limit', '50000');
        $form['fasta_download_limit'] = array(
            '#type' => 'textfield',
            '#title' => 'FASTA Download Limit',
            '#description' => 'The limit for downloading FASTA records on each query. Increase this number may add to the server load.  Set to 0 for unlimited FASTA records. (Default = 50000)',
            '#default_value' => $default_fasta_limit
        );

        $default_instruction = \Drupal::state()->get('tripal_megasearch_instruction', 'Tripal MegaSearch is a tool for downloading biological data. %limits%<br><br>Select a data type to start building your own query and download data in bulk:');
        $form['instruction'] = array(
            '#type' => 'textfield',
            '#title' => 'Form Instruction',
            '#description' => 'The instruction to show on the query form. Token %limits% can be used to show the download limits set above.',
            '#default_value' => $default_instruction,
            '#maxlength' => 1280,
            '#size' => 140
        );

        $default_sr = \Drupal::state()->get('tripal_megasearch_sequence_retrieval', ['gene' => 0, 'marker' =>0]);
        $form['sequence_retrieval'] = array(
          '#type' => 'checkboxes',
          '#title' => 'Sequence Retrieval',
          '#description' => 'Enable Sequence Retrieval for selected searches on the Static Form ',
          '#options' => array('gene' => 'Gene Search', 'marker' => 'Marker Search'),
          '#default_value' => $default_sr
        );
        
        $default_summary = \Drupal::state()->get('tripal_megasearch_data_summary', ['num_records' => 0, 'last_updated' =>0]);
        $form['data_summary'] = array(
          '#type' => 'checkboxes',
          '#title' => 'Data Summary',
          '#description' => 'Show data summary when data type was not selected. The summary will only be available after its data type page was visited once.',
          '#options' => array('num_records' => 'Show number of records for each data type.', 'last_updated' => 'Show timestamp when the data was updated (i.e. MView last updated).'),
          '#default_value' => $default_summary
        );
        
        $default_duplicates = \Drupal::state()->get('tripal_megasearch_result_duplicates', 'not_remove');
        $form['result_duplicates'] = array(
            '#type' => 'radios',
            '#title' => 'Result Page',
            '#description' => 'Remove duplicates from the results (can be very slow for large result set due to the amount of data needed to be processed). Default is not to remove duplicates. To configure this for individual dataset, add a \'duplicates\' key in the data definition conf file to override this global setting. (allowed either \'remove\' or \'not_remove\' value) ',
            '#options' => array('not_remove' => 'Do not remove duplicates (Fast)', 'remove' => 'Remove duplicates (Slow. Number of results changes according to the selected downloable fields)'),
            '#default_value' => $default_duplicates
        );

        $default_dup_limit = \Drupal::state()->get('tripal_megasearch_duplicate_limit', '1000000');
        $form['duplicate_limit'] = array(
          '#type' => 'textfield',
          '#title' => 'Duplicate Limit',
          '#description' => 'Set a limit when removing duplicates. Duplicates will only be removed if search result has fewer rows than this limit. Set to 0 for unlimited duplicate removal. (Default = 1000000)',
          '#default_value' => $default_dup_limit
        );

        $default_rows = \Drupal::state()->get('tripal_megasearch_result_rows', '10');
        $form['result_rows'] = array(
            '#type' => 'textfield',
            '#title' => 'Number of rows per result page',
            '#description' => 'The number of rows to show on each page when viewing the results. If the value is less than 1, the \'View\' result button will be disabled. (Default=10)',
            '#default_value' => $default_rows,
            '#maxlength' => 3,
            '#size' => 10
        );

        $form['submit'] = array(
            '#type' => 'submit',
            '#value' => 'Save'
        );
        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function validateForm(array &$form, FormStateInterface $form_state) {
        $values = $form_state->getValues();

    }

    /**
    * {@inheritdoc}
    */
    public function submitForm(array &$form, FormStateInterface $form_state) {
        $values = $form_state->getValues();
        \Drupal::state()->set('tripal_megasearch_query_form', $values['query_form']);
        \Drupal::state()->set('tripal_megasearch_dynamic_form_autocomplete', $values['dynamic_form_autocomplete']);
        \Drupal::state()->set('tripal_megasearch_link_in_new_tab', $values['link_in_new_tab']);
        \Drupal::state()->set('tripal_megasearch_instruction', $values['instruction']);
        \Drupal::state()->set('tripal_megasearch_data_file', $values['data_file']);
        if (is_numeric($values['download_limit'])) {
            \Drupal::state()->set('tripal_megasearch_download_limit', (int) $values['download_limit']);
        }
        if (is_numeric($values['fasta_download_limit'])) {
            \Drupal::state()->set('tripal_megasearch_fasta_download_limit', (int) $values['fasta_download_limit']);
        }
        \Drupal::state()->set('tripal_megasearch_sequence_retrieval', $values['sequence_retrieval']);
        \Drupal::state()->set('tripal_megasearch_data_summary', $values['data_summary']);
        \Drupal::state()->set('tripal_megasearch_result_duplicates', $values['result_duplicates']);
        if (is_numeric($values['duplicate_limit'])) {
            \Drupal::state()->set('tripal_megasearch_duplicate_limit', (int) $values['duplicate_limit']);
        }
        if (is_numeric($values['result_rows'])) {
            \Drupal::state()->set('tripal_megasearch_result_rows', (int) $values['result_rows']);
        }
    }
}