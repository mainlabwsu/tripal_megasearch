<?php
namespace Drupal\tripal_megasearch\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

class MainForm extends FormBase {

    /**
     * {@inheritdoc}
     */
     public function getFormId() {
        return 'tripal_megasearch_form';
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state, $search_id = NULL, $show_result = NULL) {
      $form = tripal_megasearch_form($form, $form_state);
      // Attach CSS
      $libs = $form['#attached']['library'];
      
      $default_fieldset = \Drupal::state()->get('chado_search_fieldset_behavior', 'default');
      if ($default_fieldset == 'accordion') {
        $form['#attached']['library'] = array_merge($libs, ['tripal_megasearch/tripal_megasearch', 'chado_search/chado_search_accordion']);
      }
      else if ($default_fieldset == 'open_first') {
        $form['#attached']['library'] = array_merge($libs, ['tripal_megasearch/tripal_megasearch', 'chado_search/chado_search_fieldset_open_first']);
      } else {
        $form['#attached']['library'] = array_merge($libs, ['tripal_megasearch/tripal_megasearch']);
      }
      return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function validateForm(array &$form, FormStateInterface $form_state) {

      // Rebuild the form to allow generating download files
      $form_state->setRebuild(TRUE);

      //reset any download order
      $form_build_id = $_POST['form_build_id'];
      $key = "tripal_megasearch-$form_build_id-download-order";
      $_SESSION[$key] = NULL;

      // Reset result table
      unset($form['results']['#markup']);

      // Reset Repeatable Text if selecting a different datatype
      $element = $form_state->getTriggeringElement();

      if($element && $element['#id'] == 'chado_search-id-datatype') {
        $storage = &$form_state->getStorage();
        $storage['conditions-counter'] = 1;

        $user_input = &$form_state->getUserInput();
        $form_state->unsetValue('conditions_select-0');
        $user_input['conditioins_select-0'] = 0;
        $form_state->unsetValue('conditions_op-0');
        $user_input['conditions_op-0'] = 0;
        $form_state->unsetValue('conditions_text-0');
        $user_input['conditions_text-0'] = NULL;
      }
    }

    public function submitForm(array &$form, FormStateInterface $form_state) {

    }

    public function getTitle () {
      return 'Tripal MegaSearch';
    }
}