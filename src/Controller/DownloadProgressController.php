<?php
namespace Drupal\tripal_megasearch\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;

use Drupal\Core\Controller\ControllerBase;

class DownloadProgressController extends ControllerBase {

    public function getProgress($search_id = 'tripal_megasearch') {
        $progress = \Drupal::state()->get('chado_search-' . session_id() . '-' . $search_id . '-download-progress', 0);
        return new JsonResponse(array('progress' => $progress));
    }
}