[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.3236335.svg)](https://doi.org/10.5281/zenodo.3236335)

# Tripal MegaSearch
Tripal MegaSearch is a tool for downloading biological data stored in a Tripal/Chado database. Site administrators may choose from two different data sources to serve data, i.e. Tripal MegaSearch materialized views (MViews) or Chado base tables. If neither data source is desired, administrators may also create their own MViews and serve them through Tripal MegaSearch. Site administrators can also set download limits according to their server's capability.

Tripal MegaSearch implements a flexible dynamic query form on which filters can be added dynamically. These filters are prepopulated with values mapped to the underlying data source columns so the user can filter data on each column. The user can add as many filters as s/he desires and combine them using 'AND/OR' operators.

More complex static forms are also available if the Tripal MegaSearch Mviews are used as the data source. You can adjust these MViews according to the way the data is stored in your database and populate them with the modified SQL statement. Doing so allows you to take advantages of the complex static query forms when serving data.

Tripal MegaSearch is created by Main Bioinformatics Lab (Main Lab) at Washington State University. Information about the Main Lab can be found at: https://www.bioinfo.wsu.edu

## Screenshot
![Image](theme/img/screenshot.jpg "screenshot")

## Requirement
 - Drupal 10.x or 11.x
 - Chado Search v4.2.0 or later, or the GitLab 10.x-4.x branch (https://gitlab.com/mainlabwsu/chado_search)

## Version
4.3.0

## Download
The Tripal MegaSearch module can be downloaded from GitLab:

https://gitlab.com/mainlabwsu/tripal_megasearch

## Installation

1. Download the dependency 'chado_search' module (10.x-4.x branch) into Drupal's module directory:

    ```
    cd [DRUPAL_ROOT]/modules
    git clone https://gitlab.com/mainlabwsu/chado_search
    cd chado_search
    git checkout 10.x-4.x
    ```

2. Download 'tripal_megasearch':

    ```
    cd [DRUPAL_ROOT]/modules
    git clone https://gitlab.com/mainlabwsu/tripal_megasearch
    cd tripal_megasearch
    git checkout 9.x-4.x
    ```

3. Create an empty conf file for 'chado_search' to suppress a warning about conf file not found if Chado Search is set up the first time:

    ```
    cd [DRUPAL_ROOT]/modules/chado_search/file
    touch settings.conf
    ```

4. Enable both 'chado_search' and 'tripal_megasearch' modules. This will create a set of MViews with prefix names of 'tripal_megasearch_':

    Go to: Administration > Modules, check Mainlab Chado Search and Tripal MegaSearch (under the Mainlab category) and save

    or by using the 'drush' command:

    ```
    drush pm-enable chado_search tripal_megasearch
    ```

5. Populate 'tripal_megasearch' MViews if you wish to use tripal_megasearch MViews as data source. Ignore this step if you want to use Chado base tables as data source or plan to create your own MViews:
    ```
    drush trpms-mvp --name=tripal_megasearch_contact
    drush trpms-mvp --name=tripal_megasearch_featuremap
    drush trpms-mvp --name=tripal_megasearch_gene
    drush trpms-mvp --name=tripal_megasearch_marker
    drush trpms-mvp --name=tripal_megasearch_pub
    drush trpms-mvp --name=tripal_megasearch_qtl
    drush trpms-mvp --name=tripal_megasearch_stock
    ```

    Alternatively, you can also use Tripal MView system to populate the MViews.

6. To change the settings for tripal_megasearch, use the URL below. Here you can change the data source, query form, and download limits, etc.:

    Go to: https://your.site/admin/config/mainlab/tripal_megasearch

7. To start using tripal_megasearch:

    Go to: https://your.site/tripal_megasearch
    
    Or for direct access to a specific search:
     - Contact Search: https://your.site/tripal_megasearch?datatype=tripal_megasearch_contact
     - Gene Search: https://your.site/tripal_megasearch?datatype=tripal_megasearch_gene
     - Germplasm Search: https://your.site/tripal_megasearch?datatype=tripal_megasearch_stock
     - Map Search: https://your.site/tripal_megasearch?datatype=tripal_megasearch_featuremap
     - Marker Search: https://your.site/tripal_megasearch?datatype=tripal_megasearch_marker
     - Ortholog Search: https://your.site/tripal_megasearch?datatype=tripal_megasearch_ortholog
     - Pub Search: https://your.site/tripal_megasearch?datatype=tripal_megasearch_pub
     - QTL Search: https://your.site/tripal_megasearch?datatype=tripal_megasearch_qtl

## Note
  - Please note that Tripal MegaSearch uses session. If the form becomes stale when user login or logout the website (i.e. session destroyed), the query form will need to be reset. This can be achieved by clicking the 'Reset' button on the form or by revisiting the Tripal MegaSearch URL from the browser.

  - Tripal MegaSearch uses Chado Search API and creates downloadable files in '[DRUPAL_ROOT]/sites/default/files/tripal/chado_search' directory. Site administrators may wish to clean this directory from time to time to release the space. Do not delete this direcotry or Tripal MegaSearch will not be able to create download files. Make sure the webserver (e.g. www-data) has write permission to this directory if you recreate it manually.

## Administration (Configuration > Mainlab > Tripal Megasearch)

  The following are the options you can change from the administrative interface:

 - Data Source:
   The file that defines all downloadable data as tables or materialized views (MView) in a function named 'tripal_megasearch_data_definition' which returns a PHP associative array. This file should be in the 'conf' sub-directory and have an extension of .inc. The definition will be used for creating dynamic form filters and downloadable fields. You can create your own data source file that meets above requirements which will then show up here as an option.

 - Query Form:
   There are two types of query forms. The dynamic form allows users to add filters dynamically according to the columns of a table or MView defined in the data source. The static forms are pre-defined forms that work best with the MViews preinstalled by Tripal MegaSearch. However, they may not suit everyone if data are stored in different ways. Note: If static forms are not defined, Tripal MegaSearch will fall back to use the dynamic form.

   Note: for the static forms, there is an option in the Chado Search setting to collapse the fieldsets by default. This will make these forms look shorter and more succinct. Go to 'Admin' > 'Mainlab' > 'Chado Search'. Under the 'User Interface' section, select a preferred 'Fieldset Behavior' and 'Save'.
   
 - Dynamic Form Autocomplete:
   Turn autocomplete on or off for the dynamic form. If this is on, the filters will show matching values once the user starts typing.

 - Open links in new tab:
   Open links in a new browser tab instead of the current tab.

 - Download Limit:
   The download limit on each query. Increase this number may add to the server load. Set to 0 for unlimited download. (Default = 200000)

 - FASTA Download Limit:
   The limit for downloading FASTA records on each query. Increase this number may add to the server load. Set to 0 for unlimited FASTA records. (Default = 50000)

 - Form Instruction:
   The instruction to show on the query form. Token %limits% can be used to show the download limits set above.
   
- Sequence Retrieval
  A Sequence Retrieval tool can be enabled for Gene/Marker searches on the Static forms

  ![Image](theme/img/sequence_retrieval.png "Sequence Retrieval")

- Data Summary
  Add MView stats to the landing page

  ![Image](theme/img/mview_stats.png "MView Stats")
  
 - Result Page:
   When a user clicks on the 'View' result button, remove duplicates from the results (can be very slow for large result set due to the amount of data needed to be processed). Default is not to remove duplicates. To configure this for individual dataset, add a 'duplicates' key in the data definition conf file to override this global setting. (allowed either 'remove' or 'not_remove' value).

 - Number of rows per result page:
   The number of rows to show on each page when viewing the results. If the value is less than 1, the 'View' result button will be disabled. (Default=10)

 - Recreating MViews:
   A drush command can be used to recreate default MViews. This will delete all populated data in the MView
   ```
   drush trpms-mvr --name=tripal_megasearch_stock
   ```

  - Maintenance Message:
  For long maintenance operations such as populating a big MView, you can display a maintenance message to the user by setting the 'maintenance' = TRUE in the conf file.
  
  - Short static forms:
  As more filters were added, the static forms are getting longer. Through Chado Search settings (under the User Interface section, i.e. /admin/config/mainlab/chado_search), the fieldsets can be configured to open/close like an accordion which make these forms shorter and more succinct.
  
## Performance
You can use the drush commands provided by the Chado Search module to create indexes or partition tables so the search performance can be improved if your data have grown too much and your site has encountered performance issues. The following are some example commands:
- Contact
```
drush csidxc --table=tripal_megasearch_contact --columns=lname:fname:institution:country:research_interest:type
```

- Gene/Transcript
```
drush csidxc --table=tripal_megasearch_gene --columns=name:uniquename:organism:type:analysis:genome:landmark:go:genbank
```

- Germplasm
```
drush csidxc --table=tripal_megasearch_stock --columns=name:uniquename:organism:collection:accession:maternal_parent:paternal_parent:pedigree:country:eimage_data:legend
```

- Map
```
drush csidxc --table=tripal_megasearch_featuremap --columns=name:organism:population:maternal_parent:paternal_parent:citation:category:trait:qtl:published_symbol
```

- Marker
```
drush csidxc --table=tripal_megasearch_marker --columns=uniquename:name:organism:map:mapped_organism:lg:marker_type:genome:landmark:category:trait:snp_array:pubs
```

- Ortholog/Paralog
```
drush csidxc --table=tripal_megasearch_ortholog --columns=l_genome:l_landmark:l_feature:r_genome:r_landmark
```

- Pub
```
drush csidxc --table=tripal_megasearch_pub --columns=title:volume:series_name:issue:pyear:citation:type:authors:book_name:conference_name:pub_code
```

- QTL
```
drush csidxc --table=tripal_megasearch_qtl --columns=qtl:trait:organism:type:symbol:category:map:lg:col_marker_uniquename:neighbor_marker_uniquename:population:maternal_parent:paternal_parent:citation
```

Note: see Chado Search README document for details about using the drush commands to manage indexes and partitioning tables

## Customization
 - **Changing Data Source**

    You can customize Tripal MegaSearch to include any table or materialized view and serve it to the users with a dynamic form. Tripal MegaSearch works practically with any table (or mview). Here are the steps:
    1. Create a data definition file in the [MODULE_ROOT]/conf directory. Please note that the file needs to be in the 'conf' directory and its name has to end with '.inc' You can copy the Tripal MegaSearch MView (i.e. tripal_megasearch.mviews.inc) or Chado Base Tables (i.e. chado.base_tables.inc) definition file if you want to modify it.
    ```
    cd [MODULE_ROOT]/conf
    touch my_data.inc
     ```

    2.  Edit the data definition file. The file should contain a function named 'tripal_megasearch_data_definition' and returns an associative array with the settings described in the next step.
    ```
    function tripal_megasearch_data_definition () {
      $def = array();
      return $def;
    }
    ```

    3. The returned associated array should be structured as follow:

    ```
    $def[$table] = array (                          // $table should match the table name in the database
      'name' => 'dataset_name',                     // The name to show in the Data Type dropdown
      'field' => array (                            // Fields to show in both Dynamic Form Filter dropdown and Downloadable Fields checkboxes
        $column1 => 'label1',                       // $column should match the column in the table
        $column2 => 'label2',
      )
      'field_link_callback' => array (                                // (Optional) Hyperlink callback function that returns a URL for fields
        $column1 => '$callback:$column1,$column2,...'  // $column should match the column in the table. $callback is a valid PHP callback that returns a URL for link-out. $column1, $column2, etc will be passed in to the callback as arguments
        $column2 => '$callback:$column1,$column2,...',
        ...
      )
      'form' => 'static_form_callback_function',    // (Optional) Callback for the Static Form. If this is not provided or callback does not exist, fall back to use the Dynamic Form
      'file' => 'custom_dir/custom_file.inc'        // (Optional) Include specified file when searching for the callback. Accept only relative path from the module root directory. Create custom sub-directory accordingly.
      'mview_file' => 'custom_dir/custom_mview_file.inc'    // (Optional) Use specified mview file instead of the default. Accept only relative path from the module root directory. Create custom sub-directory accordingly.
      'fasta' => 'column_to_provide_fasta_download' // (Optional) Provide a FASTA download using specified column (usually feature_id)
      'count' => 'column_to_count_unique_records'   // (Optional) Column for counting unique records (e.g. stock_id)
    )
    ```

     4. Go to the Tripal MegaSearch administrative page (https://your.site/admin/config/mainlab/tripal_megasearch) and switch the data source to use the custom file (i.e. my_data.inc)

 - **Modifying query forms**

    If you want to disable or modify any form, you can make a copy of the Data Source conf file and edit it accordingly. The following example shows you how to remove 'Country' and 'Image' filters from the static Germplasm search form.
    1. Make a copy of the 'tripal_mesasearch.mviews.inc' so we can modify it and use it as the new data source file (e. g. my_data.inc).
    ```
    cd [MODULE_ROOT]/conf
    cp tripal_mesasearch.mviews.inc my_data.inc
     ```
     2.  Edit my_data.inc and comment out the fields you don't want to show as 'Downloadable Fields' on the form with //:
   ```
    function tripal_megasearch_stock ($def = array()) {
	  $def['tripal_megasearch_stock'] = array(
	    'name' => 'Germplasm',
	    'field' => array(
	      'name' => 'Name',
	      'uniquename' => 'Unique Name',
	      'accession' => 'Accession',
	      'maternal_parent' => 'Maternal Parent',
	      'paternal_parent' => 'Paternal Parent',
	      'pedigree' => 'Pedigree',
	  //   'country' => 'Country',
	  //    'eimage_data' => 'Image Name',
	  //    'legend' => 'Legend',
	    ),
	    'form' => 'tripal_megasearch_stock_form',
	    'file' => 'includes/tripal_megasearch.form.static.stock.inc',
	    'count' => 'stock_id'
	  );
	  return $def;
	}
    ```
    3. Edit my_data.inc and change the form callback to use the version in a cutom file (i.e. 'custom/my.form.static.stock.inc')
    ```
      // 'file' => 'includes/tripal_megasearch.form.static.stock.inc',
      'file' => 'custom/my.form.static.stock.inc',
    ```

    4. Create the custom directory and file:
    ```
    cd [MODULE_ROOT]
    mkdir custom
    cp includes/tripal_megasearch.form.static.stock.inc custom/my.form.static.stock.inc
    ```

    5. Comment out or delete the form elements you don't want in the 'custom/my.form.static.stock.inc' file. In this case 'Country' and Image' filters
    ```
	Line169     // Country
				      //$form->addSelectFilter(
				      //    Set::selectFilter()
				      //    ->id('stock_country')
				      //    ->title('Country')
				      //    ->table('tripal_megasearch_stock')
				      //    ->column('country')
				      //    ->labelWidth(140)
				      //    ->fieldset('data_filters')
				      //   ->cache(TRUE)
				      //   ->newline()
				      //   );
				      //$form->addFieldset(
				      //    Set::fieldset()
				      //    ->id('stock_group_country')
				      //    ->title('Country')
				      //    ->startWidget('stock_country')
				      //    ->endWidget('stock_country')
				      //    );
				      //
				      // Image
				      //$form->addTextFilter(
				      //    Set::textFilter()
				      //    ->id('stock_eimage_data')
				      //    ->title('Name')
				      //    ->labelWidth(140)
				      //    ->fieldset('data_filters')
				      //    ->newline()
				      //    );
				      //$form->addTextFilter(
				      //    Set::textFilter()
				      //    ->id('stock_legend')
				      //    ->title('Legend')
				      //    ->labelWidth(140)
				      //    ->fieldset('data_filters')
				      //    ->newline()
				      //    );
				      //$form->addFieldset(
				      //    Set::fieldset()
				      //    ->id('stock_group_image')
				      //    ->title('Image')
				      //    ->startWidget('stock_eimage_data')
				      //    ->endWidget('stock_legend')
	Line212      //);
    ```

    6. Edit 'custom/my.form.static.stock.inc' file.  Comment out or remove those filters in the 'tripal_megasearch_ajax_stock_get_filters' function so they won't be used when submitting the form
    ```
	  function tripal_megasearch_ajax_stock_get_filters(&$form_state) {
		  $where = array();
		  $where [] = Sql::selectFilter('stock_organism', $form_state, 'organism');
		  $where [] = Sql::textFilter('stock_name', $form_state, 'name');
		  $where [] = Sql::file('stock_file', 'name', FALSE, TRUE);
		  $where [] = Sql::selectFilter('stock_collection', $form_state, 'collection');
		  $where [] = Sql::textFilter('stock_accession', $form_state, 'accession');
		  $where [] = Sql::textFilter('stock_maternal_parent', $form_state, 'maternal_parent');
		  $where [] = Sql::textFilter('stock_paternal_parent', $form_state, 'paternal_parent');
		  $where [] = Sql::textFilter('stock_pedigree', $form_state, 'pedigree');
		// $where [] = Sql::selectFilter('stock_country', $form_state, 'country');
		// $where [] = Sql::textFilter('stock_eimage_data', $form_state, 'eimage_data');
		//  $where [] = Sql::textFilter('stock_legend', $form_state, 'legend');
		  return $where;
      }
    ```

    7. Go to the Tripal MegaSearch administrative page (https://your.site/admin/config/mainlab/tripal_megasearch) and switch the data source to use the custom file (i.e. my_data.inc). Since this is a static form, you'll need to change the 'Query Form' setting to 'Static Form' in the admin interface as well. If you want to use dynamic form for other data types, just remove the 'form' and 'file' keys from 'my_data.inc'. Tripal MegaSearch will fall back to use dynamic form if a static form callback can not be found.

        Note: all of the query forms (including dynamic and static ones) are created using the Chado Seach Form API.

## Upgrade user-defined search from Drupal 7 to Drupal 9/10 (for Developers)
  1. The Mview API now requires two functions, i.e. hook_get_XXX_mview() and hook_create_XXX_mview(), to be defined. The hook_get_XXX_mview()
     should only return the SQL statement for MView population. In hook_create_XXX_mview(), the \$view_name and \$schema should be defined and 
     \$sql variable can be populated using the hook_get_XXX_mview() function. The 'chado_search_create_chado_mview(\$view_name, \$schema, \$sql)' 
     function can then be called to create the MView. 
     This is different from the Drupal 7 version of MegaSearch which has all \$view_name, \$schema and \$sql defined in the hook_create_XXX_mview() function.

  2. The API function chado_search_query in Drupal 7 will search the 'chado' schema by default which is now deprecated in Drupal 9/10. 
      If you used the API function 'chado_search_query', you'll have to change your code to accomodate the new passing arguments:
      - Drupal 7: 
      ```
        chado_search_query ($sql, $args = array(), $schema = 'chado', $append_to_path = TRUE)
      ``` 
      - Drupal 9/10:
      ```  
        chado_search_query ($sql, $args = array(), $search_path = NULL)
      ```
      
  3. Name space change for Sql and Set classes:
      - Drupal 7:
      ``` 
        use ChadoSearch\Set;
        use ChadoSearch\Sql;
      ```
      - Drupal 9/10:
      ```
        use Drupal\chado_search\Core\Set;
        use Drupal\chado_search\Core\Sql;
      ```
        
  4. Drupal Form API changes:
      - Drupal 7:
      ```
        $locus1 = $form_state['values']['between_marker_locus1'];
        form_set_error('', t('Locus1 is required.'));
      ```
      - Drupal 9/10:
      ```
        $locus1 = $form_state->getValue('between_marker_locus1');
        $form_state->setErrorByName('', 'Locus1 is required.');
      ```

  5. In Drupal 9/10, you need to include [] in the query now if you're passing array as an query argument.
     - Drupal 7:
     ```
       $sql = "SELECT distinct landmark FROM {chado_search_gene_search} WHERE analysis IN (:analysis) ORDER BY landmark";
       return chado_search_bind_dynamic_select(array(':analysis' => $val), 'landmark', $sql);
     ```
     - Drupal 9:
     ```
       $sql = "SELECT distinct landmark FROM {chado_search_gene_search} WHERE analysis IN (:analysis[]) ORDER BY landmark";
       return chado_search_bind_dynamic_select(array(':analysis[]' => $val), 'landmark', $sql);
     ```
     
  6. MView create functions now return an array of MView information for Drupal 9/Drupal 10 instead of directly creating the MView by using the 'tripal_add_mview()' function:
     - Drupal 7:
     ```
       tripal_add_mview($view_name, 'chado_search', $schema, $sql, '', FALSE);
     ```
     - Drupal 9/10:
     ```
       return array(array('view_name' => $view_name, 'schema' => $schema, 'sql' => $sql));
     ```
  
  7. SQL statements (e.g. SQL in MView definition) including semicolon ';' is not allowed in Drupal 9/10:
      - Drupal 7:
      ```
        string_agg(distinct name, ';') AS value
      ```
      - Drupal 9/10:
      ```
        string_agg(distinct name, '. ') AS value
      ```

## Problems/Suggestions
Tripal MegaSearch module is still under active development. For questions or bug
report, please contact the developers at the Main Bioinformatics Lab by emailing to:
dev@bioinfo.wsu.edu

## Citation
If you use Tripal MegaSearch, please cite:

[Jung S, Cheng CH, Buble K, Lee T, Humann J, Yu J, Crabb J, Hough H, Main D. 2021. Tripal MegaSearch: a tool for interactive and
customizable query and download of big data. Database. 10.1093/database/baab023](https://academic.oup.com/database/article/doi/10.1093/database/baab023/6253732)